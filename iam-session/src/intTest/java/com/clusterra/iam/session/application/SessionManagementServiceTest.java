/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.application;

import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.UUID;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 30.10.13
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class
)
public class SessionManagementServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private SessionManagementService sessionManagementService;

    @Autowired
    private TenantCommandService tenantCommandService;

    @Autowired
    private UserCommandService userCommandService;

    private SessionDescriptor session;

    private UserId userId;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Tenant tenant = tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
        User user = userCommandService.create(new TenantId(tenant.getId()), randomAlphabetic(10), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        userId = new UserId(user.getId());
        session = sessionManagementService.createSession(userId.getId(), UUID.randomUUID().toString(), new Date(), "1.1.1.1");
    }

    @Test
    public void test_expire_later() throws Exception {
        sessionManagementService.expireLater(session.getSessionId(), DateTime.now().plusMinutes(10).toDate());
        sessionManagementService.purgeExpired();
        SessionDescriptor s = sessionManagementService.findSessionById(session.getSessionId());
        assertThat(s, equalTo(session));
    }

    @Test
    public void test_delete_session() throws Exception {
        SessionDescriptor s = sessionManagementService.deleteSession(session.getSessionId());
        assertThat(s, equalTo(session));
        s = sessionManagementService.findSessionById(session.getSessionId());
        assertThat(s, nullValue());
    }

    @Test
    public void test_purge_expired() throws Exception {
        SessionDescriptor oneMore = sessionManagementService.createSession(userId.getId(), UUID.randomUUID().toString(), DateTime.now().plusMinutes(10).toDate(), "1.1.1.1");
        SessionDescriptor s = sessionManagementService.findSessionById(session.getSessionId());
        assertThat(s, notNullValue());
        sessionManagementService.purgeExpired();
        s = sessionManagementService.findSessionById(session.getSessionId());
        assertThat(s, nullValue());
        s = sessionManagementService.findSessionById(oneMore.getSessionId());
        assertThat(s, equalTo(oneMore));
    }

    @Test
    public void test_find_session_by_id() throws Exception {
        SessionDescriptor s = sessionManagementService.findSessionById(session.getSessionId());
        assertThat(s, equalTo(session));
    }

    @Test
    public void test_create_session() throws Exception {
        assertThat(session.getUserId(), equalTo(userId.getId()));
    }
}
