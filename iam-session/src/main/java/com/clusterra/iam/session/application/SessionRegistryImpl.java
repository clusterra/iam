/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.application;

import com.clusterra.iam.session.application.event.SessionCreatedEvent;
import com.clusterra.iam.session.application.event.SessionDeletedEvent;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;


/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class SessionRegistryImpl implements SessionRegistry {


    @Autowired
    private SessionManagementService sessionManagementService;

    private final int sessionTimeoutInSeconds;

    private final int purgePeriodInSeconds;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    public SessionRegistryImpl(int sessionTimeoutInSeconds, int purgePeriodInSeconds) {
        this.sessionTimeoutInSeconds = sessionTimeoutInSeconds;
        this.purgePeriodInSeconds = purgePeriodInSeconds;
    }


    public SessionToken registerSession(String userId, String ip) {
        String sessionId = generateSessionId();
        SessionDescriptor session = sessionManagementService.createSession(userId, sessionId, getExpirationDateFromNow(), ip);
        applicationEventPublisher.publishEvent(new SessionCreatedEvent(this, session));
        return new SessionToken(sessionId);
    }

    public SessionDescriptor findSessionDescriptor(SessionToken token) {
        return sessionManagementService.findSessionById(token.token);
    }

    public void touch(SessionToken token) {
        sessionManagementService.expireLater(token.token, getExpirationDateFromNow());
    }

    public void invalidateSession(SessionToken token) {
        SessionDescriptor sessionDescriptor = sessionManagementService.deleteSession(token.token);
        applicationEventPublisher.publishEvent(new SessionDeletedEvent(this, sessionDescriptor));
    }

    public void startPeriodicExpiration() {
        new Timer().schedule(new PurgeOldSessionsTask(), 2000, purgePeriodInSeconds * 1000);
    }


    private String generateSessionId() {
        return UUID.randomUUID().toString();
    }


    private Date getExpirationDateFromNow() {
        return DateTime.now().plusSeconds(sessionTimeoutInSeconds).toDate();
    }


    class PurgeOldSessionsTask extends TimerTask {


        public void run() {
            sessionManagementService.purgeExpired();
        }

    }
}
