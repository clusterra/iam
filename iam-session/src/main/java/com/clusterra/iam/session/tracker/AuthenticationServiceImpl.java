/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.tracker;


import com.clusterra.iam.core.application.user.UserDisabledException;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.tenant.TenantRepository;
import com.clusterra.iam.core.application.tenant.TenantDisabledException;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.crypto.EncryptionService;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.domain.model.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private EncryptionService encryptionService;


    @Override
    @Transactional
    public UserId authenticate(String login, String password) throws InvalidCredentialsException, UserDisabledException, TenantDisabledException {
        User user = userRepository.findByLoginOrEmail(login);

        if (user == null) {
            throw new InvalidCredentialsException();
        }

        if (!encryptionService.matches(password, user.getPasswordHash())) {
            throw new InvalidCredentialsException();
        }

        if (!user.isEnabled()) {
            throw new UserDisabledException(user.getLogin());
        }

        Tenant tenant = tenantRepository.findByUserId(user.getId());
        if (!tenant.isEnabled()) {
            throw new TenantDisabledException(tenant.getName());
        }

        user.updateLastLoginDate();
        user.resetPasswordToken();

        return new UserId(user.getId());
    }
}
