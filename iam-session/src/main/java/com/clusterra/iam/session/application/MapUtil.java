/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.application;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public abstract class MapUtil {


    public static Map<String, Object> fromArgList(Object ... args) {
        if ((args.length % 2) != 0) {
            throw new IllegalArgumentException("Even number of arguments must be supplied");
        }

        Map<String, Object> result = new HashMap<String, Object>(args.length >> 1);

        for (int i = 0; i < args.length - 1; i += 2) {
            if (args[i] instanceof String) {
                result.put((String)args[i], args[i + 1]);
            } else {
                throw new IllegalArgumentException("Param name must be String");
            }
        }

        return result;
    }

    public static Map<String, Object> emptyArgs() {
        return Collections.emptyMap();
    }

}
