/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.application.event;

import com.clusterra.iam.session.application.SessionDescriptor;
import org.springframework.context.ApplicationEvent;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 18.10.13
 */
public class SessionDeletedEvent extends ApplicationEvent {

    private final SessionDescriptor sessionDescriptor;

    public SessionDeletedEvent(Object source, SessionDescriptor sessionDescriptor) {
        super(source);
        this.sessionDescriptor = sessionDescriptor;
    }

    public SessionDescriptor getSessionDescriptor() {
        return sessionDescriptor;
    }
}
