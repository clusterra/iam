/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class SessionManagementServiceImpl implements SessionManagementService {

    @Autowired
    private Dao dao;

    @Transactional
    public SessionDescriptor findSessionById(String sessionId) {
        Map<String, Object> result = dao.singleResult("select id, user_id, expires_at, ip from " + SESSION_TABLE_NAME + " where id = :id", MapUtil.fromArgList("id", sessionId));

        if (result == null) {
            return null;
        }
        return new SessionDescriptor(
                (String) result.get("user_id"),
                (String) result.get("id"),
                (Date) result.get("expires_at")
        );
    }

    @Transactional
    public SessionDescriptor createSession(String userId, String sessionId, Date expiresAt, String ip) {
        dao.insert("session", MapUtil.fromArgList(
                "user_id", userId,
                "id", sessionId,
                "expires_at", expiresAt,
                "ip", ip));
        return new SessionDescriptor(userId, sessionId, expiresAt);
    }

    @Transactional
    public SessionDescriptor deleteSession(String sessionId) {
        SessionDescriptor sessionDescriptor = findSessionById(sessionId);
        dao.update("delete from " + SESSION_TABLE_NAME + " where id = :id", MapUtil.fromArgList("id", sessionId));
        return sessionDescriptor;
    }

    @Transactional
    public void expireLater(String sessionId, Date when) {
        dao.update("update " + SESSION_TABLE_NAME + " set expires_at = :expires_at where id = :id", MapUtil.fromArgList("id", sessionId, "expires_at", when));
    }

    @Transactional
    public void purgeExpired() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        dao.update("delete from " + SESSION_TABLE_NAME + " where expires_at <= :expires_at", MapUtil.fromArgList("expires_at", now));
    }
}
