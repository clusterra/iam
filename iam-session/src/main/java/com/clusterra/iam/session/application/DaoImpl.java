/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 30.10.13
 */
@Component
@Transactional
public class DaoImpl implements Dao {

    private NamedParameterJdbcTemplate template;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDaoSupport(NamedParameterJdbcDaoSupport daoSupport) {
        template = daoSupport.getNamedParameterJdbcTemplate();
    }


    @Override
    public int insert(String tableName, Map<String, ?> params) {
        StringBuilder sql = new StringBuilder(0x100);
        sql.append("INSERT INTO ").append(tableName).append(" (");
        Set<String> keySet = params.keySet();
        for (String key : keySet) {
            sql.append(key).append(",");
        }
        sql.deleteCharAt(sql.length() - 1).append(") VALUES (");
        for (String key : keySet) {
            sql.append(":").append(key).append(",");
        }
        sql.deleteCharAt(sql.length() - 1).append(")");
        return template.update(sql.toString(), params);
    }

    @Override
    public int update(String query, Map<String, Object> parameters) {
        return template.update(query, parameters);
    }

    @Override
    public Map<String, Object> singleResult(String sql, Map<String, Object> parameters) {
        List<Map<String, Object>> result = template.queryForList(sql, parameters);
        return DataAccessUtils.singleResult(result);
    }

    @Override
    public void execute(String query, AbstractLobCreatingPreparedStatementCallback callback) {
        jdbcTemplate.execute(query, callback);
    }

    @Override
    public Map<String, Object> queryForObject(String query, Map<String, Object> parameters, RowMapper<Map<String, Object>> rowMapper) {
        return template.queryForObject(query, parameters, rowMapper);
    }
}
