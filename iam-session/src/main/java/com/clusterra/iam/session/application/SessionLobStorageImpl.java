/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.session.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 18.10.13
 */
@Component
public class SessionLobStorageImpl implements SessionLobStorage {


    private static final Logger log = LoggerFactory.getLogger(SessionManagementServiceImpl.class);

    private LobHandler lobHandler = new DefaultLobHandler();

    @Autowired
    private Dao dao;

    @Transactional
    public void store(String sessionId, SessionStorageKey key, Object value) {

        Map<String, Object> data = fetchSessionData(sessionId);

        if (data == null) {
            data = new HashMap<>();
        }

        data.put(key.getKey(), value);

        storeSessionData(sessionId, data);
    }

    @Transactional
    public Object retrieve(String sessionId, SessionStorageKey key) {
        Map<String, ?> data = fetchSessionData(sessionId);

        return data == null ? null : data.get(key.getKey());
    }

    @Transactional
    public void remove(String sessionId, SessionStorageKey key) {
        Map<String, ?> data = fetchSessionData(sessionId);

        if (data != null) {
            data.remove(key.getKey());

            storeSessionData(sessionId, data);
        }
    }

    private void storeSessionData(final String sessionId, final Map<String, ?> data) {
        dao.execute("update " + SessionManagementService.SESSION_TABLE_NAME + " set data = ? where id = ?",
                new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
                    protected void setValues(PreparedStatement ps, LobCreator lobCreator) throws SQLException {
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        ObjectOutput out = null;
                        try {
                            out = new ObjectOutputStream(bos);
                            out.writeObject(data);

                            lobCreator.setBlobAsBytes(ps, 1, bos.toByteArray());
                            ps.setString(2, sessionId);
                        } catch (Exception e) {
                            log.error("Error while serializing session data", e);
                        } finally {
                            try {
                                if (out != null) {
                                    out.close();
                                }
                                bos.close();
                            } catch (IOException ignored) {
                            }
                        }

                    }
                }
        );
    }


    @SuppressWarnings("unchecked")
    private Map<String, Object> fetchSessionData(String sessionId) {

        RowMapper<Map<String, Object>> rowMapper = new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
                ObjectInputStream ois = null;

                try {
                    InputStream is = lobHandler.getBlobAsBinaryStream(rs, "data");

                    if (is != null) {
                        ois = new ObjectInputStream(is);
                    }
                    return ois == null ? null : (Map<String, Object>) ois.readObject();
                } catch (Exception e) {
                    log.error("Error while deserializing session data", e);
                } finally {
                    if (ois != null) {
                        try {
                            ois.close();
                        } catch (IOException ignored) {
                        }
                    }
                }

                return null;
            }
        };
        return dao.queryForObject("select data from " + SessionManagementService.SESSION_TABLE_NAME + " where id = :id", MapUtil.fromArgList("id", sessionId), rowMapper);
    }
}
