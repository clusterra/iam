CREATE TABLE session (
            id varchar(255) NOT NULL,
            user_id character varying(255) NOT NULL,
            expires_at timestamp without time zone NOT NULL,
            ip character varying(255) NOT NULL,
            data bytea,
            CONSTRAINT session_pkey PRIMARY KEY (id ),
            CONSTRAINT user_id_fkey FOREIGN KEY (user_id) REFERENCES iam_user (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
        )