/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.tenant.validate;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by dkuchugurov on 27.04.2014.
 */
public class EmailConstraintValidator implements ConstraintValidator<Email, String> {

    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(EmailConstraintValidator.class);

    @Override
    public void initialize(Email uu) {
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext cxt) {
        return !StringUtils.isBlank(email) && EmailValidator.getInstance(true).isValid(email);
    }
}
