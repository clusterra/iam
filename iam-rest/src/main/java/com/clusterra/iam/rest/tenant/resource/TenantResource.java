/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.tenant.resource;

import org.springframework.hateoas.ResourceSupport;

import java.util.Date;

/**
 * Created by Denis Kuchugurov on 28.05.2014.
 */
public class TenantResource extends ResourceSupport {

    private final String tenantId;
    private final String name;
    private final String description;
    private final String webSite;
    private final String location;
    private final String language;
    private final Date createdDate;
    private final Date modifiedDate;
    private final String status;
    private final String avatarUuid;

    public TenantResource(String tenantId, String name, String description, String webSite, String location, String language, Date createdDate, Date modifiedDate, String status, String avatarUuid) {
        this.tenantId = tenantId;
        this.name = name;
        this.description = description;
        this.webSite = webSite;
        this.location = location;
        this.language = language;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.status = status;
        this.avatarUuid = avatarUuid;
    }

    public String getTenantId() {
        return tenantId;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getWebSite() {
        return webSite;
    }

    public String getLocation() {
        return location;
    }

    public String getLanguage() {
        return language;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getStatus() {
        return status;
    }

    public String getAvatarUuid() {
        return avatarUuid;
    }
}
