/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.user;

import com.clusterra.iam.avatar.application.AvatarImageResizeException;
import com.clusterra.iam.avatar.application.AvatarService;
import com.clusterra.iam.avatar.domain.model.AvatarType;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.rest.avatar.AvatarSessionKey;
import com.clusterra.iam.rest.user.resource.UserResource;
import com.clusterra.iam.rest.user.resource.UserResourceAssembler;
import com.clusterra.iam.session.application.SessionLobStorage;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.avatar.application.AvatarData;
import com.clusterra.iam.avatar.application.AvatarId;
import com.clusterra.iam.avatar.application.AvatarImageConverter;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.application.user.UserNotFoundException;
import com.clusterra.iam.core.application.user.UserQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
@RestController
@RequestMapping(value = "/iam/users")
public class UserAvatarController {

    @Autowired
    private IdentityTracker identityTracker;

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private SessionLobStorage sessionLobStorage;

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private UserQueryService userQueryService;

    @Autowired
    private UserResourceAssembler userResourceAssembler;

    @RequestMapping(value = "/{id}/change/avatar", method = RequestMethod.PUT)
    public ResponseEntity<UserResource> change(@PathVariable String id,
                                               @RequestParam(required = false) String token) throws AvatarImageResizeException, UserNotFoundException, NotAuthenticatedException {
        Validate.notEmpty(token);
        byte[] imageContent = (byte[]) sessionLobStorage.retrieve(token, AvatarSessionKey.UPLOAD_AVATAR_SESSION_KEY);

        UserId userId = identityTracker.currentUser();
        if (imageContent != null) {
            try {
                AvatarData avatarData = AvatarImageConverter.getAvatarData(new ByteArrayResource(imageContent));
                AvatarId avatarId = avatarService.newAvatar(AvatarType.USER, avatarData);
                User user = userCommandService.updateAvatar(userId, avatarId.getId());
                return new ResponseEntity<>(userResourceAssembler.toResource(user), HttpStatus.OK);
            } finally {
                sessionLobStorage.remove(token, AvatarSessionKey.UPLOAD_AVATAR_SESSION_KEY);
            }
        }
        return new ResponseEntity<>(userResourceAssembler.toResource(userQueryService.findUser(userId)), HttpStatus.OK);
    }


}
