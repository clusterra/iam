/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.session;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.domain.model.user.UserRepository;
import com.clusterra.iam.session.application.SessionDescriptor;
import com.clusterra.iam.session.application.SessionRegistry;
import com.clusterra.iam.session.application.SessionToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class SessionInterceptor implements HandlerInterceptor, Ordered {

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;

    @Autowired
    private SessionTokenResolver sessionTokenResolver;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        SessionToken token = sessionTokenResolver.resolve(request);

        if (token != null) {
            SessionDescriptor sessionDescriptor = sessionRegistry.findSessionDescriptor(token);

            if (sessionDescriptor != null) {
                User user = userRepository.findOne(sessionDescriptor.getUserId());
                if (!user.isEnabled() || !user.getTenant().isEnabled()) {
                    sessionRegistry.invalidateSession(token);
                    throw new IllegalAuthenticatedAccessException("access disabled for user " + user.getLogin());
                }
                sessionRegistry.touch(token);
                identityTrackerLifeCycle.startTracking(new UserId(user.getId()), new TenantId(user.getTenant().getId()));
            }
        }
        return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        identityTrackerLifeCycle.stopTracking();
    }

}
