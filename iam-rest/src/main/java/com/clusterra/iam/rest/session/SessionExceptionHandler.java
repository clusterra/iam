/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.session;

import com.clusterra.iam.core.application.tenant.TenantDisabledException;
import com.clusterra.iam.core.application.user.UserDisabledException;
import com.clusterra.iam.session.tracker.InvalidCredentialsException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 01.12.13
 */
@ControllerAdvice
public class SessionExceptionHandler {

    @ExceptionHandler(InvalidCredentialsException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<FieldError> handle(InvalidCredentialsException exception) {
        return Arrays.asList(new FieldError("session", "credentials", exception.getMessage()));
    }

    @ExceptionHandler(UserDisabledException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public List<FieldError> handle(UserDisabledException exception) {
        return Arrays.asList(new FieldError("user", "status", exception.getMessage()));
    }

    @ExceptionHandler(TenantDisabledException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public List<FieldError> handle(TenantDisabledException exception) {
        return Arrays.asList(new FieldError("tenant", "status", exception.getMessage()));
    }

}
