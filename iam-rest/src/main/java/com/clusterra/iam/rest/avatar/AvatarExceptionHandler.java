/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.avatar;

import com.clusterra.iam.avatar.application.AvatarImageResizeException;
import com.clusterra.iam.avatar.application.AvatarNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
@ControllerAdvice
public class AvatarExceptionHandler {


    @ExceptionHandler(value = AvatarNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public List<ObjectError> handle(AvatarNotFoundException exception) {
        return Arrays.asList(new ObjectError("avatar", exception.getMessage()));
    }

    @ExceptionHandler(AvatarImageResizeException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(AvatarImageResizeException exception) {
        return Arrays.asList(new ObjectError("file", exception.getMessage()));
    }

    @ExceptionHandler(InvalidImageFileException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(InvalidImageFileException exception) {
        return Arrays.asList(new ObjectError("file", exception.getMessage()));
    }
}
