/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.avatar;

import com.clusterra.iam.session.application.SessionStorageKey;

/**
 * Created by Denis Kuchugurov
 * 06.01.14
 */
public enum AvatarSessionKey implements SessionStorageKey {

    UPLOAD_AVATAR_SESSION_KEY("upload_avatar_session_key");

    private final String key;

    AvatarSessionKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
