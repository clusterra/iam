/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.tenant;

import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.application.tenant.InvalidTenantActivationTokenException;
import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.11.13
 */
@ControllerAdvice
public class TenantExceptionHandler {

    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(BindException exception) {
        return exception.getAllErrors();
    }

    @ExceptionHandler(TenantNotFoundException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(TenantNotFoundException exception) {
        return Arrays.asList(new ObjectError("tenant", exception.getMessage()));
    }

    @ExceptionHandler(TenantAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<FieldError> handle(TenantAlreadyExistsException exception) {
        return Arrays.asList(new FieldError("tenant", "name", exception.getMessage()));
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<FieldError> handle(EmailAlreadyExistsException exception) {
        return Arrays.asList(new FieldError("tenant", "email", exception.getMessage()));
    }

    @ExceptionHandler(InvalidEmailException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<FieldError> handle(InvalidEmailException exception) {
        return Arrays.asList(new FieldError("tenant", "email", exception.getMessage()));
    }

    @ExceptionHandler(InvalidTenantNameException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<FieldError> handle(InvalidTenantNameException exception) {
        return Arrays.asList(new FieldError("tenant", "name", exception.getMessage()));
    }

    @ExceptionHandler(InvalidTenantActivationTokenException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(InvalidTenantActivationTokenException exception) {
        return Arrays.asList(new ObjectError("activationToken", exception.getMessage()));
    }


}
