/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.user;

import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.iam.core.application.user.InvalidUserActivationTokenException;
import com.clusterra.iam.core.application.user.LoginAlreadyExistsException;
import com.clusterra.iam.core.application.user.UserDisabledException;
import com.clusterra.iam.core.protect.NotAuthorizedException;
import com.clusterra.iam.rest.session.IllegalAuthenticatedAccessException;
import com.clusterra.iam.core.application.user.InvalidPasswordTokenException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.11.13
 */
@ControllerAdvice
public class UserExceptionHandler {

    @ExceptionHandler(UserDisabledException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(UserDisabledException exception) {
        return Arrays.asList(new ObjectError("user", exception.getMessage()));
    }

    @ExceptionHandler(NotAuthenticatedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public List<ObjectError> handle(NotAuthenticatedException exception) {
        return Arrays.asList(new ObjectError("identity", exception.getMessage()));
    }

    @ExceptionHandler(IllegalAuthenticatedAccessException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public List<ObjectError> handle(IllegalAuthenticatedAccessException exception) {
        return Arrays.asList(new ObjectError("identity", exception.getMessage()));
    }

    @ExceptionHandler(NotAuthorizedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public List<ObjectError> handle(NotAuthorizedException exception) {
        return Arrays.asList(new ObjectError("authority", exception.getMessage()));
    }

    @ExceptionHandler(InvalidPasswordTokenException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(InvalidPasswordTokenException exception) {
        return Arrays.asList(new ObjectError("passwordToken", exception.getMessage()));
    }

    @ExceptionHandler(LoginAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(LoginAlreadyExistsException exception) {
        return Arrays.asList(new ObjectError("login", exception.getMessage()));
    }

    @ExceptionHandler(InvalidUserActivationTokenException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public List<ObjectError> handle(InvalidUserActivationTokenException exception) {
        return Arrays.asList(new ObjectError("activationToken", exception.getMessage()));
    }


}
