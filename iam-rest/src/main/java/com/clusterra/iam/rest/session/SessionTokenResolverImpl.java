/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.session;

import com.clusterra.iam.session.application.SessionToken;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class SessionTokenResolverImpl implements SessionTokenResolver {

    private static final String TOKEN_NAME = "token";


    @Override
    public SessionToken resolve(HttpServletRequest request) {
        String token = request.getParameter(TOKEN_NAME);

        if (token == null || token.isEmpty()) {
            return null;
        }

        return new SessionToken(token);
    }

}
