/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.tenant;

import com.clusterra.iam.avatar.application.AvatarId;
import com.clusterra.iam.avatar.application.AvatarImageResizeException;
import com.clusterra.iam.avatar.application.AvatarService;
import com.clusterra.iam.avatar.domain.model.AvatarType;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.rest.avatar.AvatarSessionKey;
import com.clusterra.iam.rest.tenant.resource.TenantResourceAssembler;
import com.clusterra.iam.session.application.SessionLobStorage;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.avatar.application.AvatarImageConverter;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import com.clusterra.iam.rest.tenant.resource.TenantResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
@RestController
@RequestMapping(value = "/iam/tenants")
public class TenantAvatarController {

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private SessionLobStorage sessionLobStorage;

    @Autowired
    private TenantCommandService tenantCommandService;

    @Autowired
    private TenantQueryService tenantQueryService;

    @Autowired
    private TenantResourceAssembler tenantResourceAssembler;

    @RequestMapping(value = "/{id}/change/avatar", method = RequestMethod.PUT)
    public ResponseEntity<TenantResource> change(@PathVariable String id,
                                                 @RequestParam(required = false) String token) throws AvatarImageResizeException, TenantNotFoundException {
        Validate.notEmpty(token);
        byte[] imageContent = (byte[]) sessionLobStorage.retrieve(token, AvatarSessionKey.UPLOAD_AVATAR_SESSION_KEY);
        if (imageContent != null) {
            try {
                AvatarId avatarId = avatarService.newAvatar(AvatarType.TENANT, AvatarImageConverter.getAvatarData(new ByteArrayResource(imageContent)));
                Tenant tenant = tenantCommandService.updateAvatar(new TenantId(id), avatarId.getId());
                return new ResponseEntity<>(tenantResourceAssembler.toResource(tenant), HttpStatus.OK);
            } finally {
                sessionLobStorage.remove(token, AvatarSessionKey.UPLOAD_AVATAR_SESSION_KEY);
            }
        }
        return new ResponseEntity<>(tenantResourceAssembler.toResource(tenantQueryService.find(new TenantId(id))), HttpStatus.OK);
    }


}
