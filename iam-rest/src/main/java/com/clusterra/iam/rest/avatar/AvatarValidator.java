/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.avatar;

import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 28.12.13
 */
public abstract class AvatarValidator {

    public static void validate(MultipartFile file) throws InvalidImageFileException {

        String fileName = file.getOriginalFilename().toLowerCase();
        if (!(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png"))) {
            throw new InvalidImageFileException(file.getOriginalFilename(), "Invalid file type. Only .png, .jpg, or .jpeg supported");
        }
        try {
            BufferedImage img = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
            if (img == null || img.getWidth() == -1) {
                throw new InvalidImageFileException(file.getOriginalFilename(), "file is not an image");
            }

            if (img.getWidth() < 128 || img.getHeight() < 128) {
                throw new InvalidImageFileException(file.getOriginalFilename(), "Invalid image size, please make sure that image is bigger than 128x128 px.");
            }
        } catch (IOException e) {
            throw new InvalidImageFileException(file.getOriginalFilename(), "file is not an image");
        }
    }
}
