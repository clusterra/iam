/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.rest.user.resource;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

/**
 * Created by Denis Kuchugurov on 27.05.2014.
 */
public class UserResource extends ResourceSupport {

    private final String userId;
    private final String firstName;
    private final String lastName;
    private final String displayName;
    private final String login;
    private final String email;
    private final String avatarUuid;
    private final boolean enabled;
    private final List<String> roles;

    public UserResource(String userId, String firstName, String lastName, String displayName, String login, String email, String avatarUuid, boolean enabled, List<String> roles) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.displayName = displayName;
        this.login = login;
        this.email = email;
        this.avatarUuid = avatarUuid;
        this.enabled = enabled;
        this.roles = roles;
    }

    public String getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public String getAvatarUuid() {
        return avatarUuid;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public List<String> getRoles() {
        return roles;
    }
}
