/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application;

import com.clusterra.iam.avatar.domain.model.AvatarType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class
)
public class AvatarServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private Resource testAvatarResource;

    @Test
    public void test_default_avatar_available() throws Exception {
        assertThat(avatarService.isDefaultAvatarAvailable(AvatarType.USER), is(true));
        assertThat(avatarService.isDefaultAvatarAvailable(AvatarType.TENANT), is(true));
    }

    @Test(expectedExceptions = AvatarNotFoundException.class)
    public void test_get_avatar_by_uuid_throws_exception() throws Exception {
        avatarService.find(randomAlphabetic(5));
    }

    @Test
    public void test_update_default_avatar_USER() throws Exception {
        assertThat(avatarService.isDefaultAvatarAvailable(AvatarType.USER), is(true));
        AvatarId uuid_1 = avatarService.findDefaultAvatarId(AvatarType.USER);

        AvatarData avatarData = AvatarImageConverter.getAvatarData(testAvatarResource);

        avatarService.newDefaultAvatar(AvatarType.USER, avatarData);
        avatarService.newDefaultAvatar(AvatarType.USER, avatarData);
        avatarService.newDefaultAvatar(AvatarType.USER, avatarData);
        assertThat(avatarService.isDefaultAvatarAvailable(AvatarType.USER), is(true));
        AvatarId uuid_2 = avatarService.findDefaultAvatarId(AvatarType.USER);
        assertThat(uuid_1, not(equalTo(uuid_2)));
    }

    @Test
    public void test_update_default_avatar_CUSTOMER() throws Exception {
        assertThat(avatarService.isDefaultAvatarAvailable(AvatarType.TENANT), is(true));
        AvatarId uuid_1 = avatarService.findDefaultAvatarId(AvatarType.TENANT);

        AvatarData avatarData = AvatarImageConverter.getAvatarData(testAvatarResource);

        avatarService.newDefaultAvatar(AvatarType.TENANT, avatarData);
        avatarService.newDefaultAvatar(AvatarType.TENANT, avatarData);
        avatarService.newDefaultAvatar(AvatarType.TENANT, avatarData);
        assertThat(avatarService.isDefaultAvatarAvailable(AvatarType.TENANT), is(true));
        AvatarId uuid_2 = avatarService.findDefaultAvatarId(AvatarType.TENANT);
        assertThat(uuid_1, not(equalTo(uuid_2)));
    }
}
