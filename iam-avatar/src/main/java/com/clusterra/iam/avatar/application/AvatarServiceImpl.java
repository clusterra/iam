/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application;

import com.clusterra.iam.avatar.domain.model.AvatarType;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.avatar.domain.model.Avatar;
import com.clusterra.iam.avatar.domain.model.AvatarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
@Service
public class AvatarServiceImpl implements AvatarService {

    @Autowired
    private AvatarRepository avatarRepository;

    @Transactional
    public AvatarData find(String id) throws AvatarNotFoundException {
        Avatar avatar = avatarRepository.findOne(id);
        if (avatar == null) {
            throw new AvatarNotFoundException(id);
        }
        return new AvatarData(avatar.getImage24(), avatar.getImage48(), avatar.getImage128());
    }

    @Transactional
    public AvatarId newAvatar(AvatarType type, AvatarData avatarData) {
        Validate.notNull(avatarData, "avatarData is null");
        Validate.notNull(avatarData.getImage24(), "avatarData.image24 is null");
        Validate.notNull(avatarData.getImage48(), "avatarData.image48 is null");
        Validate.notNull(avatarData.getImage128(), "avatarData.image128 is null");
        Avatar avatar = new Avatar(type, avatarData.getImage24(), avatarData.getImage48(), avatarData.getImage128());
        avatarRepository.save(avatar);
        return new AvatarId(avatar.getId());
    }

    @Transactional
    public AvatarId newAvatar(AvatarType type, Resource resource) throws AvatarImageResizeException {
        return newAvatar(type, AvatarImageConverter.getAvatarData(resource));
    }

    @Transactional
    public AvatarId newDefaultAvatar(AvatarType type, AvatarData avatarData) {
        Avatar defaultAvatar = avatarRepository.findDefaultAvatar(type);
        if (defaultAvatar != null) {
            avatarRepository.delete(defaultAvatar);
        }

        Validate.notNull(avatarData, "avatarData is null");
        Validate.notNull(avatarData.getImage24(), "avatarData.image24 is null");
        Validate.notNull(avatarData.getImage48(), "avatarData.image48 is null");
        Validate.notNull(avatarData.getImage128(), "avatarData.image128 is null");
        Avatar avatar = new Avatar(type, avatarData.getImage24(), avatarData.getImage48(), avatarData.getImage128());
        avatar.setDefault();
        avatarRepository.save(avatar);
        return new AvatarId(avatar.getId());
    }

    @Transactional
    public AvatarId findDefaultAvatarId(AvatarType type) {
        Avatar avatar = avatarRepository.findDefaultAvatar(type);
        if (avatar == null) {
            throw new IllegalStateException("no default avatar for type <" + type + ">");
        }
        return new AvatarId(avatar.getId());
    }

    @Transactional
    public Boolean isDefaultAvatarAvailable(AvatarType type) {
        Avatar avatar = avatarRepository.findDefaultAvatar(type);
        return avatar != null;
    }
}
