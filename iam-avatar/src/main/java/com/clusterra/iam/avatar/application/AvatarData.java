/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
public class AvatarData {

    private final byte[] image24;
    private final byte[] image48;
    private final byte[] image128;


    public AvatarData(byte[] image24, byte[] image48, byte[] image128) {
        this.image24 = image24;
        this.image48 = image48;
        this.image128 = image128;
    }

    public byte[] getImage24() {
        return image24;
    }

    public byte[] getImage48() {
        return image48;
    }

    public byte[] getImage128() {
        return image128;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AvatarData that = (AvatarData) o;

        if (!Arrays.equals(image128, that.image128)) return false;
        if (!Arrays.equals(image48, that.image48)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = image48 != null ? Arrays.hashCode(image48) : 0;
        result = 31 * result + (image128 != null ? Arrays.hashCode(image128) : 0);
        return result;
    }
}
