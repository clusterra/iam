/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application.listen;

import com.clusterra.iam.avatar.application.AvatarId;
import com.clusterra.iam.avatar.application.AvatarService;
import com.clusterra.iam.avatar.domain.model.AvatarType;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserNotFoundException;
import com.clusterra.iam.core.application.user.event.UserCreatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by kepkap on 09/11/14.
 */
@Component
public class UserCreatedEvent4AvatarListener implements ApplicationListener<UserCreatedEvent> {

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private UserCommandService userCommandService;

    @Transactional(propagation = Propagation.MANDATORY)
    public void onApplicationEvent(UserCreatedEvent event) {
        AvatarId avatarId = avatarService.findDefaultAvatarId(AvatarType.USER);
        try {
            userCommandService.updateAvatar(event.getUserId(), avatarId.getId());
        } catch (UserNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
