/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application;

import com.clusterra.iam.avatar.domain.model.AvatarType;
import org.springframework.core.io.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
public interface AvatarService {

    AvatarData find(String id) throws AvatarNotFoundException;

    AvatarId findDefaultAvatarId(AvatarType type);

    AvatarId newAvatar(AvatarType type, AvatarData avatarData);

    AvatarId newAvatar(AvatarType type, Resource resource) throws AvatarImageResizeException;

    AvatarId newDefaultAvatar(AvatarType type, AvatarData avatarData);

    Boolean isDefaultAvatarAvailable(AvatarType type);

}
