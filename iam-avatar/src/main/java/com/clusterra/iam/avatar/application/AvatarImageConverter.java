/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application;

import org.imgscalr.Scalr;
import org.springframework.core.io.Resource;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
public class AvatarImageConverter {

    public static AvatarData getAvatarData(Resource resource) throws AvatarImageResizeException {
        return new AvatarData(resize(resource, 24), resize(resource, 48), resize(resource, 128));
    }

    private static byte[] resize(Resource resource, int size) throws AvatarImageResizeException {

        try {
            BufferedImage img = ImageIO.read(resource.getInputStream());
            BufferedImage resized = Scalr.resize(img, Scalr.Method.QUALITY, Scalr.Mode.FIT_EXACT, size);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(resized, "png", baos);
            return baos.toByteArray();
        } catch (IOException e) {
            throw new AvatarImageResizeException(resource, size, e);
        }
    }
}
