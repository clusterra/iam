/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application;

import org.springframework.core.io.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 01.01.14
 */
public class AvatarImageResizeException extends Exception {

    private final String resourceFileName;

    public AvatarImageResizeException(Resource resourceFileName, int size, Exception e) {
        super("exception resizing avatar <" + resourceFileName.getFilename() + "> to size <" + size + ">", e);
        this.resourceFileName = resourceFileName.getFilename();
    }

    public String getResourceFileName() {
        return resourceFileName;
    }
}
