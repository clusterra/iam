/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.domain.model;

import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
@Entity
@Table(name = "iam_avatar")
public class Avatar {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    private byte[] image24;

    @Basic
    private byte[] image48;

    @Basic
    private byte[] image128;

    @Basic
    private Boolean isDefault;

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AvatarType avatarType;

    public Avatar(AvatarType avatarType, byte[] image24, byte[] image48, byte[] image128) {
        Validate.notNull(avatarType, "avatarType is null");
        Validate.notNull(image24, "image24 is null");
        Validate.notNull(image48, "image48 is null");
        Validate.notNull(image128, "image128 is null");
        this.avatarType = avatarType;
        this.image48 = image48;
        this.image128 = image128;
        this.image24 = image24;
        this.isDefault = false;
    }

    public void setDefault() {
        this.isDefault = true;
    }

    public String getId() {
        return id;
    }

    public byte[] getImage48() {
        return image48;
    }

    public byte[] getImage128() {
        return image128;
    }

    public byte[] getImage24() {
        return image24;
    }

    Avatar() {
    }
}
