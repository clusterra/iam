/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.avatar.application;

import com.clusterra.iam.avatar.domain.model.AvatarType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 23.12.13
 */
public class DefaultAvatarServiceImpl implements DefaultAvatarService {


    private final Resource userImageResource;

    private final Resource customerImageResource;

    @Autowired
    private AvatarService avatarService;


    public DefaultAvatarServiceImpl(Resource userImageResource, Resource customerImageResource) {
        this.customerImageResource = customerImageResource;
        this.userImageResource = userImageResource;
    }

    public void saveDefaults() throws AvatarImageResizeException {
        if (!avatarService.isDefaultAvatarAvailable(AvatarType.USER)) {
            avatarService.newDefaultAvatar(AvatarType.USER, AvatarImageConverter.getAvatarData(userImageResource));
        }
        if (!avatarService.isDefaultAvatarAvailable(AvatarType.TENANT)) {
            avatarService.newDefaultAvatar(AvatarType.TENANT, AvatarImageConverter.getAvatarData(customerImageResource));
        }
    }
}
