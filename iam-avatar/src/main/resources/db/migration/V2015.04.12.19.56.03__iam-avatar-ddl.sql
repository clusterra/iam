
    create table iam_avatar (
        id varchar(255) not null,
        avatarType varchar(255) not null,
        image128 bytea,
        image24 bytea,
        image48 bytea,
        isDefault boolean,
        primary key (id)
    );
