/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.domain.model.tenant.Tenant;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 09.08.13 : 8:28
 */
public interface TenantCommandService {

    Tenant signUp(String name, String email) throws TenantAlreadyExistsException, InvalidEmailException, EmailAlreadyExistsException, InvalidTenantNameException;

    Tenant activate(String activationToken, String login, String password, String firstName, String lastName) throws InvalidTenantActivationTokenException;

    Tenant create(String name, String email) throws TenantAlreadyExistsException, InvalidTenantNameException, InvalidEmailException;

    Tenant update(TenantId tenantId, String name, String description, String webSite, String language, String location) throws TenantAlreadyExistsException, TenantNotFoundException, InvalidTenantNameException;

    void delete(TenantId tenantId) throws TenantNotFoundException;

    void enable(TenantId tenantId) throws TenantNotFoundException;

    void disable(TenantId tenantId) throws TenantNotFoundException;

    Tenant updateAvatar(TenantId tenantId, String avatarId) throws TenantNotFoundException;

    Integer purgeExpiredSignUps();
}
