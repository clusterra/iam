/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import com.clusterra.iam.core.application.tenant.event.TenantActivatedEvent;
import com.clusterra.iam.core.application.tenant.event.TenantCreatedEvent;
import com.clusterra.iam.core.application.tenant.event.TenantDeletedEvent;
import com.clusterra.iam.core.application.tenant.event.TenantDeletingEvent;
import com.clusterra.iam.core.application.tenant.event.TenantDisabledEvent;
import com.clusterra.iam.core.application.tenant.event.TenantEnabledEvent;
import com.clusterra.iam.core.application.tenant.event.TenantSignedUpEvent;
import com.clusterra.iam.core.application.tenant.event.TenantUpdatedEvent;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.tenant.TenantRepository;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Service
public class TenantCommandServiceImpl implements TenantCommandService {

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private Environment environment;

    @Transactional
    public Tenant create(String name, String email) throws TenantAlreadyExistsException, InvalidTenantNameException, InvalidEmailException {

        if (tenantRepository.findByName(name) != null) {
            throw new TenantAlreadyExistsException(name);
        }

        Tenant tenant = new Tenant(name, email);

        tenantRepository.save(tenant);
        applicationEventPublisher.publishEvent(new TenantCreatedEvent(this, new TenantId(tenant.getId())));
        return tenant;
    }

    @Transactional
    public void enable(TenantId tenantId) throws TenantNotFoundException {
        Tenant tenant = getTenant(tenantId);
        tenant.enable();
        applicationEventPublisher.publishEvent(new TenantEnabledEvent(this, tenantId));
    }

    private Tenant getTenant(TenantId tenantId) throws TenantNotFoundException {
        Tenant tenant = tenantRepository.findOne(tenantId.getId());
        if (tenant == null) {
            throw new TenantNotFoundException(tenantId);
        }
        return tenant;
    }

    @Transactional
    public void disable(TenantId tenantId) throws TenantNotFoundException {
        Tenant tenant = getTenant(tenantId);
        tenant.disable();
        applicationEventPublisher.publishEvent(new TenantDisabledEvent(this, tenantId));
    }

    @Transactional
    public void delete(TenantId tenantId) throws TenantNotFoundException {
        Tenant tenant = getTenant(tenantId);
        applicationEventPublisher.publishEvent(new TenantDeletingEvent(this, tenantId));
        tenantRepository.delete(tenant);
        applicationEventPublisher.publishEvent(new TenantDeletedEvent(this, tenantId));
    }

    @Transactional
    public Tenant signUp(String name, String email) throws TenantAlreadyExistsException, InvalidEmailException, EmailAlreadyExistsException, InvalidTenantNameException {
        if (tenantRepository.findBySignUpEmail(email) != null) {
            throw new EmailAlreadyExistsException(email);
        }

        Tenant tenant = create(name, email);

        Integer signUpTimeoutDays = environment.getRequiredProperty("clusterra.iam-core.tenant-sign-up-timeout-days", Integer.TYPE);
        tenant.setActivationToken(signUpTimeoutDays);
        tenantRepository.save(tenant);
        applicationEventPublisher.publishEvent(new TenantSignedUpEvent(this, new TenantId(tenant.getId()), tenant.getActivationToken(), email));
        return tenant;
    }

    @Transactional
    public Tenant activate(String activationToken, String login, String password, String firstName, String lastName) throws InvalidTenantActivationTokenException {

        if (StringUtils.isEmpty(activationToken)) {
            throw new InvalidTenantActivationTokenException(activationToken);
        }

        Tenant tenant = tenantRepository.findByToken(activationToken);
        if (tenant == null) {
            throw new InvalidTenantActivationTokenException(activationToken);
        }

        if (DateTime.now().minusMinutes(1).isAfter(tenant.getActivationToken().getExpiresAt().getTime())) {
            throw new InvalidTenantActivationTokenException(activationToken, tenant.getActivationToken().getExpiresAt());
        }
        tenant.enable();
        applicationEventPublisher.publishEvent(new TenantActivatedEvent(this, new TenantId(tenant.getId()), login, password, tenant.getSignUpEmail(), firstName, lastName));
        return tenant;
    }

    @Transactional
    public Tenant update(TenantId tenantId, String name, String description, String webSite, String language, String location) throws TenantAlreadyExistsException, TenantNotFoundException, InvalidTenantNameException {
        Tenant tenant = getTenant(tenantId);
        if (tenantRepository.findByName(name) != null) {
            throw new TenantAlreadyExistsException(name);
        }

        tenant.setName(name);
        tenant.setDescription(description);
        tenant.setWebSite(webSite);
        tenant.setLanguage(language);
        tenant.setLocation(location);

        applicationEventPublisher.publishEvent(new TenantUpdatedEvent(this, tenantId));
        return tenant;
    }


    @Transactional
    public Integer purgeExpiredSignUps() {

        List<Tenant> expired = tenantRepository.findExpired(DateTime.now().toDate());
        if (!expired.isEmpty()) {
            tenantRepository.delete(expired);
        }
        return expired.size();
    }

    @Transactional
    public Tenant updateAvatar(TenantId tenantId, String avatarId) throws TenantNotFoundException {
        Tenant tenant = getTenant(tenantId);
        tenant.setAvatarId(avatarId);
        return tenant;
    }

}
