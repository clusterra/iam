/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.group;

import com.clusterra.iam.core.application.group.event.GroupCreatedEvent;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.group.Group;
import com.clusterra.iam.core.domain.model.group.GroupRepository;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.tenant.TenantRepository;
import com.clusterra.iam.core.application.group.event.GroupDeletingEvent;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 25.11.13
 */
@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TenantRepository tenantRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Transactional
    public GroupDescriptor createGroup(TenantId tenantId, String groupName) throws GroupAlreadyExistsException, TenantNotFoundException {
        Tenant tenant = tenantRepository.findOne(tenantId.getId());
        if (tenant == null) {
            throw new TenantNotFoundException(tenantId);
        }

        if (isGroupNameTaken(tenantId, groupName)) {
            throw new GroupAlreadyExistsException(tenantId, groupName);
        }

        return create(tenantId, groupName);
    }

    private GroupDescriptor create(TenantId tenantId, String groupName) {
        Tenant tenant = tenantRepository.findOne(tenantId.getId());
        Group group = groupRepository.save(new Group(tenant, groupName));
        GroupDescriptor groupDescriptor = new GroupDescriptor(group.getId(), group.getName());

        applicationEventPublisher.publishEvent(new GroupCreatedEvent(this, groupDescriptor, tenantId));
        return groupDescriptor;
    }

    @Transactional
    public GroupDescriptor findOrCreateGroup(TenantId tenantId, String groupName) {
        if (isGroupNameTaken(tenantId, groupName)) {
            Group group = groupRepository.findByTenantAndName(tenantId.getId(), groupName);
            return new GroupDescriptor(group.getId(), group.getName());
        }
        return create(tenantId, groupName);
    }

    @Transactional
    public GroupDescriptor findGroupByName(TenantId tenantId, String groupName) throws GroupNotFoundException {
        Group group = groupRepository.findByTenantAndName(tenantId.getId(), groupName);
        if (group == null) {
            throw new GroupNotFoundException(tenantId, groupName);
        }
        return new GroupDescriptor(group.getId(), group.getName());
    }

    @Transactional
    public boolean isGroupNameTaken(TenantId tenantId, String groupName) {
        return groupRepository.findByTenantAndName(tenantId.getId(), groupName) != null;
    }

    @Transactional
    public void deleteByTenantId(TenantId tenantId) {
        Set<Group> groups = groupRepository.findByTenantId(tenantId.getId());
        for (Group group : groups) {
            applicationEventPublisher.publishEvent(new GroupDeletingEvent(this, new GroupDescriptor(group.getId(), group.getName())));
            groupRepository.delete(group);
        }
    }
}
