/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user;

import org.apache.commons.lang3.StringUtils;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.domain.model.user.UserRepository;
import com.clusterra.iam.core.domain.model.user.spec.ActivatedUserSpecification;
import com.clusterra.iam.core.domain.model.user.spec.InvitedUserSpecification;
import com.clusterra.iam.core.domain.model.user.spec.SearchByUserSpecification;
import com.clusterra.iam.core.domain.model.user.spec.TenantUserSpecification;
import com.clusterra.iam.core.protect.Protected;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 19.12.13
 */
@Service
public class UserQueryServiceImpl implements UserQueryService {

    @Autowired
    private UserRepository userRepository;

    @Transactional
    @Protected(roles = {DefaultRole.USER, DefaultRole.ADMIN})
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Transactional
    public User findByPasswordToken(String passwordToken) throws InvalidPasswordTokenException {
        if (StringUtils.isEmpty(passwordToken)) {
            throw new InvalidPasswordTokenException(passwordToken);
        }
        User user = userRepository.findByPasswordToken(passwordToken);

        if (user == null) {
            throw new InvalidPasswordTokenException(passwordToken);
        }
        return user;
    }

    @Transactional
    public User findByActivationToken(String activationToken) throws InvalidUserActivationTokenException {
        if (StringUtils.isEmpty(activationToken)) {
            throw new InvalidUserActivationTokenException(activationToken);
        }
        User user = userRepository.findByActivationToken(activationToken);
        if (user == null) {
            throw new InvalidUserActivationTokenException(activationToken);
        }
        return user;
    }

    @Transactional
    public void checkActivationToken(String activationToken) throws InvalidUserActivationTokenException {
        findByActivationToken(activationToken);
    }

    @Transactional
    @Protected(roles = {DefaultRole.USER, DefaultRole.ADMIN})
    public User findUser(UserId userId) throws UserNotFoundException {
        User user = userRepository.findOne(userId.getId());
        if (user == null) {
            throw new UserNotFoundException(userId);
        }
        return user;
    }

    @Transactional
    @Protected(roles = {DefaultRole.USER, DefaultRole.ADMIN})
    public Page<User> findActivated(TenantId tenantId, Pageable pageable, String searchBy) {
        Specification<User> tenantSpec = new TenantUserSpecification(tenantId);
        Specifications<User> specifications = Specifications.where(tenantSpec).and(new ActivatedUserSpecification());

        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new SearchByUserSpecification(searchBy));
        }

        return userRepository.findAll(specifications, pageable);
    }

    @Transactional
    @Protected(roles = {DefaultRole.USER, DefaultRole.ADMIN})
    public Page<User> findInvited(TenantId tenantId, Pageable pageable, String searchBy) {
        Specification<User> tenantSpec = new TenantUserSpecification(tenantId);
        Specifications<User> specifications = Specifications.where(tenantSpec).and(new InvitedUserSpecification());

        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new SearchByUserSpecification(searchBy));
        }

        return userRepository.findAll(specifications, pageable);
    }

    @Transactional
    @Protected(roles = {DefaultRole.USER, DefaultRole.ADMIN})
    public Page<User> findAll(TenantId tenantId, Pageable pageable, String searchBy) {
        Specification<User> tenantSpec = new TenantUserSpecification(tenantId);
        Specifications<User> specifications = Specifications.where(tenantSpec);

        if (!StringUtils.isEmpty(searchBy)) {
            specifications = specifications.and(new SearchByUserSpecification(searchBy));
        }
        return userRepository.findAll(specifications, pageable);
    }

    @Transactional
    public Integer getTotalCount(TenantId tenantId) {
        return userRepository.getTotalUsersCount(tenantId.getId());
    }

}