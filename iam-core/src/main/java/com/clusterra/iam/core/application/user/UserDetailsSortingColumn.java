/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.12.13
 */
public enum UserDetailsSortingColumn {

    userId("id"),
    firstName("person.firstName"),
    lastName("person.lastName"),
    login("login"),
    email("person.contactInformation.email"),
    status("status");

    private final String entityField;

    UserDetailsSortingColumn(String entityField) {
        this.entityField = entityField;
    }

    public String getEntityField() {
        return entityField;
    }
}
