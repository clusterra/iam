/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.protect;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface Protected {

    /**
     * Specify a name of an action to be protected. The name is just a logical one,
     * agreed with domain experts and not tied to any method or class name.
     * At least one of current user's roles should be associated with that name in order to
     * execute a method protected with this annotation
     *
     * @return Array of action names
     */
    String[] actions() default {};

    /**
     * Specify a role a user should have in order to
     * execute a method protected with this annotation
     *
     * @return Array of role names
     */
    String[] roles() default {};

}
