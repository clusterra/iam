/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user.listen;

import com.clusterra.iam.core.application.group.DefaultGroup;
import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.event.UserActivatedEvent;
import com.clusterra.iam.core.application.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Denis Kuchugurov on 28.05.2014.
 */
@Component
public class UserActivatedEvent4UserRoleListener implements ApplicationListener<UserActivatedEvent> {

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Transactional
    public void onApplicationEvent(UserActivatedEvent event) {

        RoleDescriptor role = roleService.findOrCreateRole(event.getTenantId(), DefaultRole.USER);
        GroupDescriptor groupDefault = groupService.findOrCreateGroup(event.getTenantId(), DefaultGroup.DEFAULT.getName());
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(event.getTenantId(), event.getUserId(), role, groupDefault);
    }


}
