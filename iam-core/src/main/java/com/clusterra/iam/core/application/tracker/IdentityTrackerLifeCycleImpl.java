/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tracker;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.UserId;
import org.springframework.stereotype.Component;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class IdentityTrackerLifeCycleImpl implements IdentityTracker, IdentityTrackerLifeCycle {

    private static final ThreadLocal<UserId> currentUser = new ThreadLocal<>();

    private static final ThreadLocal<TenantId> currentTenant = new ThreadLocal<>();

    @Override
    public UserId currentUser() throws NotAuthenticatedException {
        UserId userId = currentUser.get();
        if (userId == null) {
            throw new NotAuthenticatedException();
        }
        return userId;
    }

    @Override
    public TenantId currentTenant() throws NotAuthenticatedException {
        TenantId tenantId = currentTenant.get();
        if (tenantId == null) {
            throw new NotAuthenticatedException();
        }
        return tenantId;
    }

    @Override
    public void startTracking(UserId userId, TenantId tenantId) {
        currentUser.set(userId);
        currentTenant.set(tenantId);
    }


    @Override
    public void stopTracking() {
        currentUser.remove();
        currentTenant.remove();
    }

}
