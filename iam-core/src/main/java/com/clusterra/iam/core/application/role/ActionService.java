/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

import java.util.Set;

/**
 * @author Alexander Fyodorov
 *
 */
public interface ActionService {

    ActionDescriptor createAction(String actionName) throws ActionAlreadyExistsException;

    void deleteAction(ActionDescriptor actionDescriptor);

    ActionDescriptor findActionByName(String actionName);

    Set<ActionDescriptor> findAllActions();

    Set<ActionDescriptor> findActionsByRole(RoleDescriptor roleDescriptor);

    Set<RoleDescriptor> findRolesByAction(ActionDescriptor actionDescriptor);

    void allowActionForRole(ActionDescriptor actionDescriptor, RoleDescriptor roleDescriptor) throws ActionAlreadyAllowedException;

    void disallowActionForRole(ActionDescriptor actionDescriptor, RoleDescriptor roleDescriptor);

    boolean isActionAllowed(ActionDescriptor actionDescriptor, RoleDescriptor roleDescriptor);
}
