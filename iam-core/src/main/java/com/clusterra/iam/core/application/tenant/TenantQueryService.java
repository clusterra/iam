/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Denis Kuchugurov
 * 09.01.14
 */
public interface TenantQueryService {

    boolean isNameTaken(String name);

    boolean isEnabled(TenantId tenantId) throws TenantNotFoundException;

    Tenant find(TenantId tenantId) throws TenantNotFoundException;

    Tenant findByUser(UserId userId) throws TenantNotFoundException;

    Tenant findByName(String name) throws TenantNotFoundException;

    Page<Tenant> findAll(Pageable pageable, String searchBy);

    Integer getTotalCount();

    void checkActivationToken(String activationToken) throws InvalidTenantActivationTokenException;
}
