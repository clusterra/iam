/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.protect;

import com.clusterra.iam.core.application.role.AuthorizationService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.iam.core.application.user.UserId;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Aspect
@Component
public class ProtectedInterceptor implements Ordered {

    @Autowired
    private IdentityTracker identityTracker;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private AuthorizationService authorizationService;

    private int order = 1;


    @Before("@within(aProtected)")
    public void forType(Protected aProtected) throws Throwable {
        checkAuthority(aProtected);
    }

    @Before("@annotation(aProtected)")
    public void forMethod(Protected aProtected) throws Throwable {
        checkAuthority(aProtected);
    }

    private void checkAuthority(Protected aProtected) throws NotAuthenticatedException {
        UserId userId = identityTracker.currentUser();

        if (aProtected.actions() == null) {
            checkAction(userId, aProtected.actions());
        } else {
            checkRole(userId, aProtected.roles());
        }
    }

    private void checkAction(UserId userId, String[] actionNames) {
        for (String actionName : actionNames) {
            if (authorizationService.canExecute(userId, actionName)) {
                return;
            }
        }
        throw new NotAuthorizedException(actionNames);
    }

    private void checkRole(UserId userId, String[] roleNames) {
        List<RoleDescriptor> roles = authorizedMembershipService.findRolesByUser(userId);
        for (RoleDescriptor role : roles) {
            if (Arrays.asList(roleNames).contains(role.getRoleName())) {
                return;
            }
        }
        throw new NotAuthorizedException(roleNames);
    }


    /**
     * Allow overriding of the default order.
     *
     * @param order aspect order
     */
    public void setOrder(int order) {
        this.order = order;
    }


    @Override
    public int getOrder() {
        return order;
    }
}
