/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.membership;

import com.clusterra.iam.core.domain.model.role.Role;
import com.clusterra.iam.core.domain.model.user.User;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.domain.model.group.Group;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Entity
@Table(name = "iam_authorized_membership", uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "role_id", "group_id"}))
public class AuthorizedMembership {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Role role;

    @ManyToOne
    private Group group;


    public AuthorizedMembership(User user, Role role, Group group) {
        Validate.notNull(role, "role is null");
        Validate.notNull(user, "user is null");
        Validate.notNull(group, "group is null");

        this.user = user;
        this.role = role;
        this.group = group;
    }


    public String getId() {
        return id;
    }


    public User getUser() {
        return user;
    }


    public Role getRole() {
        return role;
    }

    public Group getGroup() {
        return group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthorizedMembership that = (AuthorizedMembership) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


    AuthorizedMembership() {
    } // for ORM
}
