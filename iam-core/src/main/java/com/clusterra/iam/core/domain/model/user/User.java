/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.user;

import com.clusterra.iam.core.domain.model.Token;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Entity
@Table(name = "iam_user")
public class User {


    private static final String LOGIN_PATTERN = "^(.){3,50}$";


    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(unique = true)
    private String login;

    @Basic
    private String passwordHash;

    @Basic
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "token", column = @Column(name = "activationToken")),
            @AttributeOverride(name = "expiresAt", column = @Column(name = "activationTokenExpiresAt"))
    })
    private Token activationToken;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "token", column = @Column(name = "passwordToken")),
            @AttributeOverride(name = "expiresAt", column = @Column(name = "passwordTokenExpiresAt"))
    })
    private Token passwordToken;

    @Embedded
    private Person person;

    @ManyToOne
    private Tenant tenant;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @LastModifiedDate
    private Date lastModifiedDate;

    @Basic
    @LastModifiedBy
    private String lastModifiedByUserId;


    @Basic
    private Date lastLoginDate;


    public User(String email, Tenant tenant) {
        setTenant(tenant);
        this.person = new Person(new ContactInformation(email));
        disable();
    }


    public void setActivationToken() {
        this.activationToken = new Token(DateTime.now().plusDays(5).toDate());
        this.status = UserStatus.INVITED;
    }

    public Token getActivationToken() {
        return activationToken;
    }

    public void resetActivationToken() {
        this.activationToken = null;
    }

    public void resetPasswordToken() {
        this.passwordToken = null;
    }

    public void setPasswordToken() {
        this.passwordToken = new Token(DateTime.now().plusDays(5).toDate());
    }

    public Token getPasswordToken() {
        return passwordToken;
    }

    public void setCredentials(String login, String passwordHash) {
        setLogin(login);
        this.passwordHash = passwordHash;
    }

    public void setFirstName(String firstName) {
        this.person.setFirstName(firstName);
    }

    public void setLastName(String lastName) {
        this.person.setLastName(lastName);
    }

    public void setAvatarId(String avatarId) {
        this.person.setAvatarId(avatarId);
    }

    public void updateLastLoginDate() {
        this.lastLoginDate = new Date();
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }


    public void changeEmail(String email) {
        this.person.getContactInformation().setEmail(email);
    }

    public String getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public boolean isEnabled() {
        return status.equals(UserStatus.ENABLED);
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void enable() {
        this.status = UserStatus.ENABLED;
    }


    public void updatePassword(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void disable() {
        this.status = UserStatus.DISABLED;
    }

    public Person getPerson() {
        return person;
    }

    public Tenant getTenant() {
        return tenant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id.equals(user.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void setLogin(String login) {
        login = login.trim();
        Validate.notBlank(login, "login is blank");
        Validate.matchesPattern(login, LOGIN_PATTERN, "Login " + login + " does not match pattern:" + LOGIN_PATTERN);

        this.login = login.toLowerCase();
    }

    private void setTenant(Tenant tenant) {
        Validate.notNull(tenant, "tenant is null");
        this.tenant = tenant;
    }

    protected User() {
    } // for ORM

    public Boolean hasActivationToken() {
        return activationToken != null;
    }
}
