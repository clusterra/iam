/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user.event;

import com.clusterra.iam.core.application.user.UserId;
import org.springframework.context.ApplicationEvent;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class UserEmailUpdatedEvent extends ApplicationEvent {

    private final UserId userId;

    private final String oldEmail;
    private final String newEmail;


    public UserEmailUpdatedEvent(Object source, UserId userId, String oldEmail, String newEmail) {
        super(source);
        this.userId = userId;
        this.oldEmail = oldEmail;
        this.newEmail = newEmail;
    }


    public UserId getUserId() {
        return userId;
    }


    public String getOldEmail() {
        return oldEmail;
    }


    public String getNewEmail() {
        return newEmail;
    }

}
