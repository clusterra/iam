/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.membership;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.membership.event.AuthorizedMembershipCreatedEvent;
import com.clusterra.iam.core.application.membership.event.AuthorizedMembershipDeletedEvent;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.group.GroupRepository;
import com.clusterra.iam.core.domain.model.membership.AuthorizedMembership;
import com.clusterra.iam.core.domain.model.role.Role;
import com.clusterra.iam.core.domain.model.role.RoleRepository;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.domain.model.user.UserRepository;
import com.clusterra.iam.core.domain.model.group.Group;
import com.clusterra.iam.core.domain.model.membership.AuthorizedMembershipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.12.13
 */
@Service
public class AuthorizedMembershipServiceImpl implements AuthorizedMembershipService {

    @Autowired
    private AuthorizedMembershipRepository authorizedMembershipRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Transactional
    public void createAuthorizedMembershipIfNotExists(TenantId tenantId, UserId userId, RoleDescriptor roleDescriptor, GroupDescriptor groupDescriptor) {
        checkTenantMatch(tenantId, userId);

        AuthorizedMembership membership = authorizedMembershipRepository.findByUserIdAndRoleId(userId.getId(), roleDescriptor.getRoleId(), groupDescriptor.getGroupId());

        if (membership == null) {
            Role r = roleRepository.findOne(roleDescriptor.getRoleId());
            User u = userRepository.findOne(userId.getId());
            Group g = groupRepository.findOne(groupDescriptor.getGroupId());
            AuthorizedMembership authorizedMembership = new AuthorizedMembership(u, r, g);
            authorizedMembershipRepository.save(authorizedMembership);
            applicationEventPublisher.publishEvent(new AuthorizedMembershipCreatedEvent(this, tenantId, roleDescriptor, userId, groupDescriptor));
        }
    }

    @Transactional
    public void deleteAuthorizedMembership(TenantId tenantId, UserId userId, RoleDescriptor roleDescriptor, GroupDescriptor groupDescriptor) {
        checkTenantMatch(tenantId, userId);

        AuthorizedMembership authorizedMembership = authorizedMembershipRepository.findByUserIdAndRoleId(userId.getId(), roleDescriptor.getRoleId(), groupDescriptor.getGroupId());

        if (authorizedMembership != null) {
            authorizedMembershipRepository.delete(authorizedMembership);
            applicationEventPublisher.publishEvent(new AuthorizedMembershipDeletedEvent(this, tenantId, roleDescriptor, userId, groupDescriptor));
        }
    }

    @Transactional
    public List<UserId> findUsersByGroup(GroupDescriptor groupDescriptor) {
        List<User> users = authorizedMembershipRepository.findUsersByGroupId(groupDescriptor.getGroupId());

        List<UserId> result = new ArrayList<>(users.size());
        for (User user : users) {
            result.add(new UserId(user.getId()));
        }
        return result;
    }

    @Transactional
    public List<UserId> findUsersByGroupAndRole(GroupDescriptor groupDescriptor, RoleDescriptor roleDescriptor) {
        List<User> users = authorizedMembershipRepository.findUsersByGroupIdAndRoleId(groupDescriptor.getGroupId(), roleDescriptor.getRoleId());

        List<UserId> result = new ArrayList<>(users.size());
        for (User user : users) {
            result.add(new UserId(user.getId()));
        }
        return result;
    }

    @Transactional
    public List<RoleDescriptor> findRolesByGroupAndUser(GroupDescriptor groupDescriptor, UserId userId) {
        List<Role> roles = authorizedMembershipRepository.findRolesByGroupIdAndUserId(groupDescriptor.getGroupId(), userId.getId());

        List<RoleDescriptor> result = new ArrayList<>(roles.size());
        for (Role role : roles) {
            result.add(new RoleDescriptor(role.getId(), role.getName()));
        }
        return result;
    }

    @Transactional
    public List<RoleDescriptor> findRolesByUser(UserId userId) {
        List<Role> roles = authorizedMembershipRepository.findRolesByUserId(userId.getId());

        List<RoleDescriptor> result = new ArrayList<>(roles.size());
        for (Role role : roles) {
            result.add(new RoleDescriptor(role.getId(), role.getName()));
        }
        return result;
    }

    @Transactional
    public void deleteByUserId(UserId userId) {
        Set<AuthorizedMembership> memberships = authorizedMembershipRepository.findByUserId(userId.getId());
        for (AuthorizedMembership membership : memberships) {
            authorizedMembershipRepository.delete(membership);
        }
    }

    @Transactional
    public void deleteByGroup(GroupDescriptor groupDescriptor) {
        Set<AuthorizedMembership> memberships = authorizedMembershipRepository.findByGroupId(groupDescriptor.getGroupId());
        for (AuthorizedMembership membership : memberships) {
            authorizedMembershipRepository.delete(membership);
        }
    }

    private void checkTenantMatch(TenantId tenantId, UserId userId) {
        User user = userRepository.findOne(userId.getId());
        if (!user.getTenant().getId().equals(tenantId.getId())) {
            throw new WrongTenantException(tenantId, userId);
        }
    }
}
