/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.role;


import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Entity
@Table(name = "iam_authorization", uniqueConstraints = @UniqueConstraint(columnNames = {"role_id", "action_id"}))
public class Authorization {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @ManyToOne
    private Role role;

    @ManyToOne
    private Action action;


    public Authorization(Role role, Action action) {
        Validate.notNull(role, "role is null");
        Validate.notNull(action, "action is null");

        this.role = role;
        this.action = action;
    }


    public String getId() {
        return id;
    }


    public Role getRole() {
        return role;
    }


    public Action getAction() {
        return action;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Authorization that = (Authorization) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


    Authorization() {
    } // for ORM
}
