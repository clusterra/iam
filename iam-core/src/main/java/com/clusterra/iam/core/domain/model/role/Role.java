/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.role;

import com.clusterra.iam.core.domain.model.tenant.Tenant;
import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Entity
@Table(name = "iam_role")
public class Role {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column
    private String name;

    @ManyToOne
    private Tenant tenant;

    public Role(Tenant tenant, String name) {
        setTenant(tenant);
        setName(name);
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Tenant getTenant() {
        return tenant;
    }

    private void setTenant(Tenant tenant) {
        Validate.notNull(tenant, "tenant is null");
        this.tenant = tenant;
    }

    private void setName(String name) {
        Validate.notBlank(name, "name is blank");
        name = name.trim();
        Validate.matchesPattern(name, "^[a-zA-Z0-9._\\-\\s]{3,50}$", name + " role name is incorrect");
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;
        return id.equals(role.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    Role() {
    } // for ORM
}
