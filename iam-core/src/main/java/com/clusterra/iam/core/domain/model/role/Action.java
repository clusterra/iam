/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.role;

import org.apache.commons.lang3.Validate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Entity
@Table(name = "iam_action")
public class Action {
    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(unique = true)
    private String name;


    public Action(String name) {
        setName(name);
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Action action = (Action) o;

        return id.equals(action.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


    private void setName(String name) {
        Validate.notBlank(name, "name is blank");

        name = name.trim();

        Validate.matchesPattern(name, "^[a-zA-Z0-9._\\-\\s]{3,50}$", "Action name is incorrect");

        this.name = name;
    }


    Action() {
    } // for ORM
}
