/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.tenant;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public interface TenantRepository extends PagingAndSortingRepository<Tenant, String>, JpaSpecificationExecutor<Tenant> {


    @Query("select t from Tenant t where t.name = ?1")
    Tenant findByName(String name);

    @Query("select t from Tenant t where t.id = (select u.tenant.id from User u where u.id = ?1)")
    Tenant findByUserId(String userId);

    @Query("select t from Tenant t where t.activationToken.token = ?1")
    Tenant findByToken(String token);

    @Query("select t from Tenant t where t.activationToken.expiresAt < ?1")
    List<Tenant> findExpired(Date now);

    @Query("select count(t) from Tenant t")
    Integer getTotalCount();

    @Query("select t from Tenant t where t.signUpEmail = ?1")
    Tenant findBySignUpEmail(String email);
}
