/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.membership;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.UserId;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.12.13
 */
public interface AuthorizedMembershipService {

    void createAuthorizedMembershipIfNotExists(TenantId tenantId, UserId userId, RoleDescriptor roleDescriptor, GroupDescriptor groupDescriptor);

    void deleteAuthorizedMembership(TenantId tenantId, UserId userId, RoleDescriptor roleDescriptor, GroupDescriptor groupDescriptor);

    List<UserId> findUsersByGroup(GroupDescriptor groupDescriptor);

    List<UserId> findUsersByGroupAndRole(GroupDescriptor groupDescriptor, RoleDescriptor roleDescriptor);

    List<RoleDescriptor> findRolesByGroupAndUser(GroupDescriptor groupDescriptor, UserId userId);

    List<RoleDescriptor> findRolesByUser(UserId userId);

    void deleteByUserId(UserId userId);

    void deleteByGroup(GroupDescriptor groupDescriptor);
}
