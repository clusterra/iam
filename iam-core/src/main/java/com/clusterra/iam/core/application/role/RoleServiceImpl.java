/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.membership.AuthorizedMembershipRepository;
import com.clusterra.iam.core.domain.model.role.Role;
import com.clusterra.iam.core.domain.model.role.RoleRepository;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.tenant.TenantRepository;
import com.clusterra.iam.core.application.role.event.RoleCreatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class RoleServiceImpl implements RoleService {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private RoleRepository roleRepository;


    @Autowired
    private TenantRepository tenantRepository;


    @Autowired
    private AuthorizedMembershipRepository authorizedMembershipRepository;


    @Transactional
    public RoleDescriptor createRole(TenantId tenantId, String roleName) throws RoleAlreadyExistsException {
        if (roleRepository.findByTenantAndName(tenantId.getId(), roleName) != null) {
            throw new RoleAlreadyExistsException(tenantId, roleName);
        }
        return create(tenantId, roleName);
    }

    private RoleDescriptor create(TenantId tenantId, String roleName) {
        Tenant t = tenantRepository.findOne(tenantId.getId());
        Role role = new Role(t, roleName);

        roleRepository.save(role);
        RoleDescriptor roleDescriptor = new RoleDescriptor(role.getId(), role.getName());
        applicationEventPublisher.publishEvent(new RoleCreatedEvent(this, tenantId, roleDescriptor));
        return roleDescriptor;
    }

    @Transactional
    public RoleDescriptor findOrCreateRole(TenantId tenantId, String roleName) {
        if (isRoleNameTaken(tenantId, roleName)) {
            Role role = roleRepository.findByTenantAndName(tenantId.getId(), roleName);
            return new RoleDescriptor(role.getId(), role.getName());
        }
        return create(tenantId, roleName);
    }

    @Transactional
    public void deleteRole(RoleDescriptor roleDescriptor) {
        roleRepository.delete(roleRepository.findOne(roleDescriptor.getRoleId()));
    }

    @Transactional
    public Set<RoleDescriptor> findAllRoles(TenantId tenantId) {
        Set<Role> roles = roleRepository.findAll(tenantId.getId());

        Set<RoleDescriptor> result = new HashSet<>(roles.size());

        for (Role role : roles) {
            result.add(new RoleDescriptor(role.getId(), role.getName()));
        }

        return result;
    }

    @Transactional
    public void deleteByTenantId(TenantId tenantId) {
        Set<Role> roles = roleRepository.findAll(tenantId.getId());
        for (Role role : roles) {
            roleRepository.delete(role);
        }
    }

    @Transactional
    public RoleDescriptor findRoleByName(TenantId tenantId, String roleName) throws RoleNotFoundException {
        Role role = roleRepository.findByTenantAndName(tenantId.getId(), roleName);
        if (role == null) {
            throw new RoleNotFoundException(tenantId, roleName);
        }

        return new RoleDescriptor(role.getId(), role.getName());
    }

    @Transactional
    public boolean isRoleNameTaken(TenantId tenantId, String roleName) {
        return roleRepository.findByTenantAndName(tenantId.getId(), roleName) != null;
    }

}
