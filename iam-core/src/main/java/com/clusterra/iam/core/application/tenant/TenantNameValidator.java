/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * Created by Denis Kuchugurov
 * 08.01.14
 */
public abstract class TenantNameValidator {

    public static String validate(String name) throws InvalidTenantNameException {
        name = name.trim();
        if (StringUtils.isBlank(name)) {
            throw new InvalidTenantNameException(name);
        }

        try {
            Validate.matchesPattern(name, "^[a-zA-Z][a-zA-Z0-9]{2,49}$", "Tenant name is incorrect");
        } catch (IllegalArgumentException e) {
            throw new InvalidTenantNameException(name, e);
        }

        return name;
    }

}
