/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.testutil;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 09.08.13 : 22:47
 */
public abstract class UserRandomUtil {

    public static String getRandomEmail() {
        return randomAlphabetic(5) + "@" + randomAlphabetic(5) + ".org";
    }

    public static String randomPassword() {
        return randomAlphabetic(3).toLowerCase() + randomAlphabetic(3).toUpperCase() + "_" + Integer.toString(20);
    }
}