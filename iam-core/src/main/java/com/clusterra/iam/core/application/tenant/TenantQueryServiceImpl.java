/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.tenant.TenantRepository;
import com.clusterra.iam.core.domain.model.tenant.spec.TenantSearchBySpecification;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Denis Kuchugurov
 * 10.01.14
 */
@Service
public class TenantQueryServiceImpl implements TenantQueryService {

    @Autowired
    private TenantRepository tenantRepository;

    @Transactional
    public boolean isNameTaken(String name) {
        return tenantRepository.findByName(name) != null;
    }

    @Transactional
    public Tenant findByUser(UserId userId) throws TenantNotFoundException {
        Tenant tenant = tenantRepository.findByUserId(userId.getId());
        if (tenant == null) {
            throw new TenantNotFoundException(userId);
        }
        return tenant;
    }

    @Transactional
    public Tenant findByName(String name) throws TenantNotFoundException {
        Tenant tenant = tenantRepository.findByName(name);
        if (tenant == null) {
            throw new TenantNotFoundException(name);
        }
        return tenant;
    }

    @Transactional
    public boolean isEnabled(TenantId tenantId) throws TenantNotFoundException {
        Tenant tenant = tenantRepository.findOne(tenantId.getId());
        if (tenant == null) {
            throw new TenantNotFoundException(tenantId);
        }
        return tenant.isEnabled();
    }

    @Transactional
    public Tenant find(TenantId tenantId) throws TenantNotFoundException {
        Tenant tenant = tenantRepository.findOne(tenantId.getId());
        if (tenant == null) {
            throw new TenantNotFoundException(tenantId);
        }
        return tenant;
    }

    @Transactional
    public Page<Tenant> findAll(Pageable pageable, String searchBy) {

        if (!StringUtils.isEmpty(searchBy)) {
            TenantSearchBySpecification specification = new TenantSearchBySpecification(searchBy);
            return tenantRepository.findAll(specification, pageable);
        } else {
            return tenantRepository.findAll(pageable);
        }
    }

    @Transactional
    public Integer getTotalCount() {
        return tenantRepository.getTotalCount();
    }

    @Transactional
    public void checkActivationToken(String activationToken) throws InvalidTenantActivationTokenException {
        if (StringUtils.isEmpty(activationToken)) {
            throw new InvalidTenantActivationTokenException(activationToken);
        }
        Tenant tenant = tenantRepository.findByToken(activationToken);
        if (tenant == null) {
            throw new InvalidTenantActivationTokenException(activationToken);
        }

        if (DateTime.now().isAfter(tenant.getActivationToken().getExpiresAt().getTime())) {
            throw new InvalidTenantActivationTokenException(activationToken, tenant.getActivationToken().getExpiresAt());
        }
    }
}
