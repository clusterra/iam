/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 09.08.13 : 8:28
 */public class InvalidTenantActivationTokenException extends Exception {

    private final String invalidToken;

    public InvalidTenantActivationTokenException(String invalidToken) {
        super("invalid tenant activation token <" + invalidToken + ">");
        this.invalidToken = invalidToken;
    }

    public InvalidTenantActivationTokenException(String token, Date expiredAd) {
        super("invalid tenant activation token <" + token + ">, expired at <" + expiredAd + ">");
        this.invalidToken = token;
    }

    public String getInvalidToken() {
        return invalidToken;
    }
}
