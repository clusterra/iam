/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant.event;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.Token;
import org.springframework.context.ApplicationEvent;

/**
 * Created by Denis Kuchugurov
 * 08.01.14
 */
public class TenantSignedUpEvent extends ApplicationEvent {

    private final TenantId tenantId;

    private final Token activationToken;

    private final String email;

    public TenantSignedUpEvent(Object source, TenantId tenantId, Token activationToken, String email) {
        super(source);
        this.tenantId = tenantId;
        this.activationToken = activationToken;
        this.email = email;
    }

    public TenantId getTenantId() {
        return tenantId;
    }

    public Token getActivationToken() {
        return activationToken;
    }

    public String getEmail() {
        return email;
    }
}
