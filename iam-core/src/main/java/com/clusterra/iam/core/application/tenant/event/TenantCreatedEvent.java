/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant.event;

import com.clusterra.iam.core.application.tenant.TenantId;
import org.springframework.context.ApplicationEvent;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 07.08.13 : 15:59
 */
public class TenantCreatedEvent extends ApplicationEvent {

    private final TenantId tenantId;

    public TenantCreatedEvent(Object source, TenantId tenantId) {
        super(source);
        this.tenantId = tenantId;
    }

    public TenantId getTenantId() {
        return tenantId;
    }
}
