/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

import com.clusterra.iam.core.domain.model.role.Action;
import com.clusterra.iam.core.domain.model.role.ActionRepository;
import com.clusterra.iam.core.domain.model.role.Authorization;
import com.clusterra.iam.core.domain.model.role.AuthorizationRepository;
import com.clusterra.iam.core.domain.model.role.Role;
import com.clusterra.iam.core.domain.model.role.RoleRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class ActionServiceImpl implements ActionService {

    @Autowired
    private ActionRepository actionRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AuthorizationRepository authorizationRepository;


    @Transactional
    public ActionDescriptor createAction(String actionName) throws ActionAlreadyExistsException {
        if (actionRepository.findByName(actionName) != null) {
            throw new ActionAlreadyExistsException(actionName);
        }
        Action action = new Action(actionName);
        actionRepository.save(action);
        return new ActionDescriptor(action.getId(), action.getName());
    }


    @Transactional
    public void deleteAction(ActionDescriptor actionDescriptor) {
        actionRepository.delete(actionRepository.findOne(actionDescriptor.getActionId()));
    }

    @Transactional
    public Set<ActionDescriptor> findAllActions() {
        Iterable<Action> actions = actionRepository.findAll();
        Set<ActionDescriptor> result = new HashSet<>();
        for (Action action : actions) {
            result.add(new ActionDescriptor(action.getId(), action.getName()));
        }
        return result;
    }


    @Transactional
    public void allowActionForRole(ActionDescriptor actionDescriptor, RoleDescriptor roleDescriptor) throws ActionAlreadyAllowedException {
        Validate.notNull(actionDescriptor, "action is null, but required");
        Validate.notNull(roleDescriptor, "role is null, but required");
        if (isActionAllowed(actionDescriptor, roleDescriptor)) {
            throw new ActionAlreadyAllowedException(actionDescriptor, roleDescriptor);
        }
        Role r = roleRepository.findOne(roleDescriptor.getRoleId());
        Action a = actionRepository.findOne(actionDescriptor.getActionId());

        Authorization authorization = new Authorization(r, a);

        authorizationRepository.save(authorization);
    }


    @Transactional
    public void disallowActionForRole(ActionDescriptor actionDescriptor, RoleDescriptor roleDescriptor) {
        Validate.notNull(actionDescriptor, "action is null, but required");
        Validate.notNull(roleDescriptor, "role is null, but required");
        Authorization authorization = authorizationRepository.findByActionIdAndRoleId(actionDescriptor.getActionId(), roleDescriptor.getRoleId());

        if (authorization != null) {
            authorizationRepository.delete(authorization);
        }
    }

    @Transactional
    public Set<ActionDescriptor> findActionsByRole(RoleDescriptor roleDescriptor) {
        Validate.notNull(roleDescriptor, "role is null, but required");
        Set<Authorization> authorizations = authorizationRepository.findByRoleId(roleDescriptor.getRoleId());

        Set<ActionDescriptor> result = new HashSet<>(authorizations.size());

        for (Authorization authorization : authorizations) {
            result.add(new ActionDescriptor(authorization.getAction().getId(), authorization.getAction().getName()));
        }

        return result;
    }

    @Transactional
    public Set<RoleDescriptor> findRolesByAction(ActionDescriptor actionDescriptor) {
        Set<Authorization> authorizations = authorizationRepository.findByActionId(actionDescriptor.getActionId());

        Set<RoleDescriptor> result = new HashSet<>(authorizations.size());

        for (Authorization authorization : authorizations) {
            Role role = authorization.getRole();
            result.add(new RoleDescriptor(role.getId(), role.getName()));
        }

        return result;
    }


    @Transactional
    public boolean isActionAllowed(ActionDescriptor actionDescriptor, RoleDescriptor roleDescriptor) {
        Authorization authorization = authorizationRepository.findByActionIdAndRoleId(actionDescriptor.getActionId(), roleDescriptor.getRoleId());
        return authorization != null;
    }

    @Transactional
    public ActionDescriptor findActionByName(String actionName) {
        Action action = actionRepository.findByName(actionName);
        return action == null ? null : new ActionDescriptor(action.getId(), action.getName());
    }
}
