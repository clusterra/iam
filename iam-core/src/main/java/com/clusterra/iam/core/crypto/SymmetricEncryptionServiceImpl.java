/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.crypto;


import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class SymmetricEncryptionServiceImpl implements SymmetricEncryptionService {
    private static final String cipherType = "AES/ECB/PKCS5Padding";

    private final SecretKeySpec keySpec;


    public SymmetricEncryptionServiceImpl(String secret) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA");
            digest.update(Utf8.encode(secret));
            this.keySpec = new SecretKeySpec(digest.digest(), 0, 16, "AES");
        } catch (NoSuchAlgorithmException nsae) {
            throw new RuntimeException("Key generating algorithm not found", nsae);
        }
    }


    @Override
    public String encrypt(String raw) {
        try {
            Cipher aes = Cipher.getInstance(cipherType);
            aes.init(Cipher.ENCRYPT_MODE, keySpec);
            return new String(Hex.encode(aes.doFinal(raw.getBytes())));
        } catch (Exception e) {
            throw new RuntimeException("Encrypting failed", e);
        }
    }


    @Override
    public String decrypt(String encrypted) {
        try {
            Cipher aes = Cipher.getInstance(cipherType);
            aes.init(Cipher.DECRYPT_MODE, keySpec);
            return new String(aes.doFinal(Hex.decode(encrypted)));
        } catch (Exception e) {
            throw new RuntimeException("Encrypting failed", e);
        }
    }

}
