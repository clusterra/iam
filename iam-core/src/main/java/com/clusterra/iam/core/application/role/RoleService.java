/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;


import com.clusterra.iam.core.application.tenant.TenantId;

import java.util.Set;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public interface RoleService {

    RoleDescriptor createRole(TenantId tenantId, String roleName) throws RoleAlreadyExistsException;

    RoleDescriptor findOrCreateRole(TenantId tenantId, String roleName);

    void deleteRole(RoleDescriptor roleDescriptor);

    RoleDescriptor findRoleByName(TenantId tenantId, String roleName) throws RoleNotFoundException;

    boolean isRoleNameTaken(TenantId tenantId, String roleName);

    Set<RoleDescriptor> findAllRoles(TenantId tenantId);

    void deleteByTenantId(TenantId tenantId);
}
