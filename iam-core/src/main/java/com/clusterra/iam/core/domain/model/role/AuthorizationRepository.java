/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.role;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public interface AuthorizationRepository extends CrudRepository<Authorization, String> {

    @Query("select a from Authorization a where a.action.id = ?1")
    Set<Authorization> findByActionId(String actionId);

    @Query("select a from Authorization a where a.role.id = ?1")
    Set<Authorization> findByRoleId(String roleId);

    @Query("select a from Authorization a where a.action.id = ?1 AND a.role.id = ?2")
    Authorization findByActionIdAndRoleId(String actionId, String roleId);
}
