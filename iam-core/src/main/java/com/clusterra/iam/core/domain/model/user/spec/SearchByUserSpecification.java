/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.user.spec;

import com.clusterra.iam.core.domain.model.user.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.12.13
 */
public class SearchByUserSpecification implements Specification<User> {

    private final String searchBy;

    public SearchByUserSpecification(String searchBy) {
        this.searchBy = ("%" + searchBy + "%").toLowerCase();
    }

    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

        Predicate firstName = cb.like(cb.lower(root.<String>get("person").get("firstName").as(String.class)), searchBy);
        Predicate lastName = cb.like(cb.lower(root.<String>get("person").get("lastName").as(String.class)), searchBy);
        Predicate email = cb.like(cb.lower(root.<String>get("person").get("contactInformation").get("email").as(String.class)), searchBy);

        return cb.or(firstName, lastName, email);
    }
}
