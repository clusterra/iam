/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.user;

import org.apache.commons.lang3.Validate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Value Object
 *
 * @author Alexander Fyodorov
 */
@Embeddable
public class ContactInformation {
    @Basic
    @Column(unique = true)
    private String email;


    public ContactInformation(String email) {
        email = email.trim();
        Validate.notEmpty(email);
        this.email = email.toLowerCase();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        Validate.notNull(email, "email is null");
        email = email.trim().toLowerCase();
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContactInformation that = (ContactInformation) o;

        return email.equals(that.email);

    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }


    ContactInformation() {
    } // for ORM
}
