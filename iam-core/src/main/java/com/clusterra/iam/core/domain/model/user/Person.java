/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.user;

import org.apache.commons.lang3.Validate;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;


/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Embeddable
public class Person {

    private static final String NAME_PATTERN = "^[а-яА-ЯёЁa-zA-Z0-9._\\-\\s]{1,50}$";

    @Embedded
    private ContactInformation contactInformation;

    @Basic
    private String firstName;

    @Basic
    private String lastName;

    @Basic
    private String avatarId;


    public Person(ContactInformation contactInformation) {
        Validate.notNull(contactInformation, "contactInformation is null");

        this.contactInformation = contactInformation;
    }


    public String getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(String avatarId) {
        Validate.notNull(avatarId, "avatarId is null");
        this.avatarId = avatarId;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        Validate.notBlank(firstName, "firstName is blank");
        firstName = firstName.trim();
        Validate.matchesPattern(firstName, NAME_PATTERN, firstName + " first name is incorrect (doesn't matches pattern: " + NAME_PATTERN);
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        Validate.notBlank(lastName, "lastName is blank");
        lastName = lastName.trim();
        Validate.matchesPattern(lastName, NAME_PATTERN, lastName + " last name is incorrect (doesn't matches pattern: " + NAME_PATTERN);
        this.lastName = lastName;
    }

    Person() {
    } // for ORM

    public String getDisplayName() {
        return firstName + " " + lastName;
    }
}
