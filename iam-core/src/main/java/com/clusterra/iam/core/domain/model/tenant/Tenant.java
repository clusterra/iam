/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.tenant;

import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.EmailValidor;
import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantNameValidator;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.domain.model.Token;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Entity
@Table(name = "iam_tenant")
public class Tenant {

    @Id
    @GeneratedValue(generator = "base64")
    @GenericGenerator(name = "base64", strategy = "com.clusterra.hibernate.base64.Base64IdGenerator")
    private String id;

    @Basic
    @Column(unique = true)
    private String name;

    @Basic
    private String description;

    @Basic
    private String webSite;

    @Basic
    private String location;

    @Basic
    private String language;

    @Basic
    @Enumerated(EnumType.STRING)
    private TenantStatus status;

    @Basic
    @CreatedDate
    private Date createdDate;

    @Basic
    @CreatedBy
    private String createdByUserId;

    @Basic
    @LastModifiedDate
    private Date lastModifiedDate;

    @Basic
    @LastModifiedBy
    private String lastModifiedByUserId;

    @Basic
    private String avatarId;

    @Embedded
    private Token activationToken;

    @Basic
    @Column(unique = true)
    private String signUpEmail;

    public Tenant(String name, String signUpEmail) throws InvalidTenantNameException, InvalidEmailException {

        setName(name);
        this.signUpEmail = EmailValidor.validate(signUpEmail);
        enable();
    }

    public void setActivationToken(int signUpTimeoutDays) {
        this.activationToken = new Token(DateTime.now().plusDays(signUpTimeoutDays).toDate());
        this.status = TenantStatus.SIGNED_UP;
    }

    public Token getActivationToken() {
        return activationToken;
    }

    public void enable() {
        this.status = TenantStatus.ENABLED;
        this.activationToken = null;
    }

    public void disable() {
        this.status = TenantStatus.DISABLED;
    }

    public String getId() {
        return id;
    }

    public boolean isEnabled() {
        return this.status.equals(TenantStatus.ENABLED);
    }

    public String getName() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tenant tenant = (Tenant) o;

        return id.equals(tenant.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void setName(String name) throws InvalidTenantNameException {
        Validate.notBlank(name, "Tenant name can not be blank");
        name = name.trim();
        this.name = TenantNameValidator.validate(name);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    Tenant() {
    }

    public String getDescription() {
        return description;
    }

    public String getWebSite() {
        return webSite;
    }

    public String getLocation() {
        return location;
    }

    public String getLanguage() {
        return language;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }

    public TenantStatus getStatus() {
        return status;
    }

    public String getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(String avatarId) {
        this.avatarId = avatarId;
    }

    public String getSignUpEmail() {
        return signUpEmail;
    }
}
