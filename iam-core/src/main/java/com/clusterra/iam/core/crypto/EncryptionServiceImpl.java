/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.crypto;


import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * Heavily borrowed from org.springframework.security.crypto.password.StandardPasswordEncoder
 * An implementation that uses SHA-256 hashing with 1024 iterations and a
 * random 8-byte random salt value. It uses an additional system-wide secret value to provide additional protection.
 * The digest algorithm is invoked on the concatenated bytes of the salt, secret and password.
 *
 * @author Keith Donald
 * @author Luke Taylor
 * @author Alexander Fyodorov
 */
@Service
public class EncryptionServiceImpl implements EncryptionService {

    private static final int DEFAULT_ITERATIONS = 1024;

    private static final String DEFAULT_PASSWORD_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$";

    private final Digester digester;

    @Autowired
    private Environment environment;


    private final SecureRandomBytesKeyGenerator saltGenerator;


    private EncryptionServiceImpl() {
        this.digester = new Digester("SHA-256", DEFAULT_ITERATIONS);
        this.saltGenerator = new SecureRandomBytesKeyGenerator();
    }


    public String encrypt(String raw) {
        Validate.matchesPattern(raw, DEFAULT_PASSWORD_PATTERN, "Password must have at least one capital letter, one small letter and one digit, be at least 8 characters, max 20");
        return encrypt(raw, saltGenerator.generateKey());
    }

    public boolean matches(String raw, String encrypted) {
        byte[] digested = decrypt(encrypted);
        byte[] salt = EncodingUtils.subArray(digested, 0, saltGenerator.getKeyLength());
        return matches(digested, digest(raw, salt));
    }

    private String encrypt(String raw, byte[] salt) {
        byte[] digest = digest(raw, salt);
        return new String(Hex.encode(digest));
    }


    private byte[] digest(String raw, byte[] salt) {
        String key = environment.getRequiredProperty("clusterra.iam-core.encryption-key");
        byte[] digest = digester.digest(EncodingUtils.concatenate(salt, Utf8.encode(key), Utf8.encode(raw)));
        return EncodingUtils.concatenate(salt, digest);
    }


    private byte[] decrypt(String encrypted) {
        return Hex.decode(encrypted);
    }

    /**
     * Constant time comparison to prevent against timing attacks.
     *
     * @param expected expected byte array
     * @param actual   actual byte array
     * @return boolean result
     */
    private boolean matches(byte[] expected, byte[] actual) {
        if (expected.length != actual.length) {
            return false;
        }

        int result = 0;
        for (int i = 0; i < expected.length; i++) {
            result |= expected[i] ^ actual[i];
        }
        return result == 0;
    }

}
