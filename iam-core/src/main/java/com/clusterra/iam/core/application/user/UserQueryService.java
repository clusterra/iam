/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 19.12.13
 */
public interface UserQueryService {

    User findByPasswordToken(String passwordToken) throws InvalidPasswordTokenException;

    void checkActivationToken(String activationToken) throws InvalidUserActivationTokenException;

    User findByEmail(String email);

    User findByActivationToken(String activationToken) throws InvalidUserActivationTokenException;

    User findUser(UserId userId) throws UserNotFoundException;

    Page<User> findActivated(TenantId tenantId, Pageable pageable, String searchBy);

    Page<User> findInvited(TenantId tenantId, Pageable pageable, String searchBy);

    Page<User> findAll(TenantId tenantId, Pageable pageable, String searchBy);

    Integer getTotalCount(TenantId tenantId);

}
