/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public interface UserCommandService {


    User create(TenantId tenantId, String login, String email, String password, String firstName, String lastName) throws EmailAlreadyExistsException, InvalidEmailException, LoginAlreadyExistsException, TenantNotFoundException;

    void delete(UserId userId) throws UserNotFoundException;

    User invite(TenantId tenantId, String email) throws EmailAlreadyExistsException, InvalidEmailException, TenantNotFoundException;

    User activate(String activationToken, String login, String password, String firstName, String lastName) throws InvalidUserActivationTokenException, LoginAlreadyExistsException;

    void forgotPassword(String loginOrEmail) throws UserDisabledException;

    User resetPassword(String passwordToken, String password) throws InvalidPasswordTokenException, UserDisabledException;

    User changePassword(UserId userId, String password) throws UserNotFoundException;

    User enable(UserId userId) throws UserNotFoundException;

    User disable(UserId userId) throws UserNotFoundException;

    void updateLogin(UserId userId, String login) throws LoginAlreadyExistsException, UserNotFoundException;

    void updateEmail(UserId userId, String email) throws EmailAlreadyExistsException, InvalidEmailException, UserNotFoundException;

    User updateName(UserId userId, String firstName, String lastName) throws UserNotFoundException;

    User updateAvatar(UserId userId, String avatarId) throws UserNotFoundException;

    User cancelInvitation(UserId userId) throws UserNotFoundException;

    void deleteByTenantId(TenantId tenantId);
}
