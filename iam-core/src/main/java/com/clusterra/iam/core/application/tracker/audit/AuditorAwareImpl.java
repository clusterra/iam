/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tracker.audit;

import com.clusterra.iam.core.application.tracker.NotAuthenticatedException;
import com.clusterra.iam.core.application.tracker.IdentityTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class AuditorAwareImpl implements AuditorAware<String> {

    private static Logger logger = LoggerFactory.getLogger(AuditorAwareImpl.class);

    @Autowired
    private IdentityTracker identityTracker;

    @Override
    public String getCurrentAuditor() {
        try {
            return identityTracker.currentUser().getId();
        } catch (NotAuthenticatedException e) {
            logger.debug("current identity not set, but called for, message: {}", e.getLocalizedMessage());
            return null;
        }
    }
}
