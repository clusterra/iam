/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.membership;

import com.clusterra.iam.core.domain.model.role.Role;
import com.clusterra.iam.core.domain.model.user.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public interface AuthorizedMembershipRepository extends CrudRepository<AuthorizedMembership, Long> {

    @Query("select am from AuthorizedMembership am where am.user.id = ?1")
    Set<AuthorizedMembership> findByUserId(String userId);

    @Query("select am from AuthorizedMembership am where am.role.id = ?1")
    Set<AuthorizedMembership> findByRoleId(String roleId);

    @Query("select am from AuthorizedMembership am where am.group.id = ?1")
    Set<AuthorizedMembership> findByGroupId(String groupId);

    @Query("select distinct am.user from AuthorizedMembership am where am.group.id = ?1")
    List<User> findUsersByGroupId(String groupId);

    @Query("select am.user from AuthorizedMembership am where am.group.id = ?1 and am.role.id = ?2")
    List<User> findUsersByGroupIdAndRoleId(String groupId, String roleId);

    @Query("select am.role from AuthorizedMembership am where am.group.id = ?1 and am.user.id = ?2")
    List<Role> findRolesByGroupIdAndUserId(String groupId, String userId);

    @Query("select am from AuthorizedMembership am where am.user.id = ?1 and am.role.id = ?2 and am.group.id = ?3")
    AuthorizedMembership findByUserIdAndRoleId(String userId, String roleId, String groupId);

    @Query("select am from AuthorizedMembership am, Authorization a where am.role.id = a.role.id and am.user.id = ?1 and a.action.name = ?2")
    AuthorizedMembership isUserAssociatedWithAction(String userId, String actionName);

    @Query("select am.role from AuthorizedMembership am where am.user.id = ?1")
    List<Role> findRolesByUserId(String userId);
}
