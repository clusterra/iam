/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.validator.routines.EmailValidator;
import com.clusterra.iam.core.application.user.InvalidEmailException;

/**
 * Created by Denis Kuchugurov
 * 08.01.14
 */
public abstract class EmailValidor {

    public static String validate(String email) throws InvalidEmailException {
        email = email.trim();
        Validate.notEmpty(email, "email is empty");
        if (StringUtils.isBlank(email)) {
            throw new InvalidEmailException(email);
        }
        if (!EmailValidator.getInstance(true).isValid(email)) {
            throw new InvalidEmailException(email);
        }
        return email;
    }

}
