/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.membership.AuthorizedMembershipRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class AuthorizationServiceImpl implements AuthorizationService {


    @Autowired
    private AuthorizedMembershipRepository authorizedMembershipRepository;


    @Transactional
    public boolean canExecute(UserId userId, ActionDescriptor actionDescriptor) {
        return authorizedMembershipRepository.isUserAssociatedWithAction(userId.getId(), actionDescriptor.getActionName()) != null;
    }

    @Transactional
    public boolean canExecute(UserId userId, String actionName) {
        Validate.notNull(userId);
        Validate.notEmpty(actionName);
        return authorizedMembershipRepository.isUserAssociatedWithAction(userId.getId(), actionName) != null;
    }

}
