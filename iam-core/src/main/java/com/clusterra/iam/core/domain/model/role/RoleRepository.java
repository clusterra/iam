/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.domain.model.role;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;


/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public interface RoleRepository extends CrudRepository<Role, String> {


    @Query("select r from Role r where r.tenant.id = ?1 and r.name = ?2")
    Role findByTenantAndName(String tenantId, String name);

    @Query("select r from Role r where r.tenant.id = ?1")
    Set<Role> findAll(String tenantId);
}
