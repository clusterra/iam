/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user;

import com.clusterra.iam.core.application.EmailValidor;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.event.PasswordChangedEvent;
import com.clusterra.iam.core.application.user.event.UserDeletedEvent;
import com.clusterra.iam.core.application.user.event.UserDeletingEvent;
import com.clusterra.iam.core.application.user.event.UserDisplayNameUpdatedEvent;
import com.clusterra.iam.core.application.user.event.UserLoginUpdatedEvent;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import com.clusterra.iam.core.application.user.event.ChangePasswordRequestedEvent;
import com.clusterra.iam.core.application.user.event.UserActivatedEvent;
import com.clusterra.iam.core.application.user.event.UserCreatedEvent;
import com.clusterra.iam.core.application.user.event.UserDisabledEvent;
import com.clusterra.iam.core.application.user.event.UserEmailUpdatedEvent;
import com.clusterra.iam.core.application.user.event.UserEnabledEvent;
import com.clusterra.iam.core.application.user.event.UserInvitedEvent;
import com.clusterra.iam.core.crypto.EncryptionService;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.domain.model.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@Component
public class UserCommandServiceImpl implements UserCommandService {

    private static final Logger log = LoggerFactory.getLogger(UserCommandServiceImpl.class);

    @Autowired
    private TenantQueryService tenantQueryService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EncryptionService encryptionService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    @Transactional
    public User invite(TenantId tenantId, String email) throws EmailAlreadyExistsException, InvalidEmailException, TenantNotFoundException {
        User user = createUser(tenantId, email);

        user.setActivationToken();

        userRepository.save(user);

        applicationEventPublisher.publishEvent(new UserInvitedEvent(this, tenantId, new UserId(user.getId()), email, user.getActivationToken().getToken()));
        return user;
    }


    @Transactional
    public User activate(String activationToken, String login, String password, String firstName, String lastName) throws InvalidUserActivationTokenException, LoginAlreadyExistsException {

        if (StringUtils.isEmpty(activationToken)) {
            throw new InvalidUserActivationTokenException(activationToken);
        }

        User user = userRepository.findByActivationToken(activationToken);
        if (user == null) {
            throw new InvalidUserActivationTokenException(activationToken);
        }

        if (userRepository.findByLogin(login) != null) {
            throw new LoginAlreadyExistsException(login);
        }
        user.setFirstName(firstName);
        user.setLastName(lastName);

        user.setCredentials(login, encryptionService.encrypt(password));
        user.enable();
        user.resetActivationToken();
        UserId userId = new UserId(user.getId());

        applicationEventPublisher.publishEvent(new UserActivatedEvent(this, user.getPerson().getContactInformation().getEmail(), login, new TenantId(user.getTenant().getId()), userId));
        return user;
    }

    @Transactional
    public User resetPassword(String passwordToken, String password) throws InvalidPasswordTokenException, UserDisabledException {
        User user = userRepository.findByPasswordToken(passwordToken);

        if (user == null) {
            throw new InvalidPasswordTokenException(passwordToken);
        }

        if (!user.isEnabled()) {
            throw new UserDisabledException(user.getLogin());
        }

        setPassword(user, password);
        return user;
    }

    private void setPassword(User user, String password) {
        user.updatePassword(encryptionService.encrypt(password));
        user.resetPasswordToken();
        String email = user.getPerson().getContactInformation().getEmail();
        applicationEventPublisher.publishEvent(new PasswordChangedEvent(this, new UserId(user.getId()), email));
    }

    @Transactional
    public User changePassword(UserId userId, String password) throws UserNotFoundException {
        User user = getUser(userId);

        setPassword(user, password);
        return user;
    }

    @Transactional
    public User enable(UserId userId) throws UserNotFoundException {
        User user = getUser(userId);
        user.enable();
        applicationEventPublisher.publishEvent(new UserEnabledEvent(this, userId));
        return user;
    }


    @Transactional
    public User create(TenantId tenantId, String login, String email, String password, String firstName, String lastName) throws EmailAlreadyExistsException, InvalidEmailException, LoginAlreadyExistsException, TenantNotFoundException {
        User user = createUser(tenantId, email);

        if (userRepository.findByLogin(login) != null) {
            throw new LoginAlreadyExistsException(login);
        }

        user.setCredentials(login, encryptionService.encrypt(password));
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.enable();
        userRepository.save(user);
        return user;
    }

    private User createUser(TenantId tenantId, String email) throws InvalidEmailException, EmailAlreadyExistsException, TenantNotFoundException {
        checkEmail(email);

        Tenant tenant = tenantQueryService.find(tenantId);
        User user = new User(EmailValidor.validate(email), tenant);

        userRepository.save(user);
        applicationEventPublisher.publishEvent(new UserCreatedEvent(this, tenantId, new UserId(user.getId())));
        return user;
    }

    @Transactional
    public void delete(UserId userId) throws UserNotFoundException {
        User user = getUser(userId);
        delete(user);
    }

    private void delete(User user) {
        applicationEventPublisher.publishEvent(new UserDeletingEvent(this, new UserId(user.getId())));
        userRepository.delete(user);
        applicationEventPublisher.publishEvent(new UserDeletedEvent(this, new UserId(user.getId())));
    }

    @Transactional
    public User disable(UserId userId) throws UserNotFoundException {
        User user = getUser(userId);
        user.disable();
        applicationEventPublisher.publishEvent(new UserDisabledEvent(this, userId));
        return user;
    }

    @Transactional
    public void updateLogin(UserId userId, String login) throws LoginAlreadyExistsException, UserNotFoundException {
        User user = getUser(userId);

        String oldLogin = user.getLogin();
        if (oldLogin.equals(login)) {
            return;
        }

        if (userRepository.findByLogin(login) != null) {
            throw new LoginAlreadyExistsException(login);
        }

        user.setLogin(login);
        applicationEventPublisher.publishEvent(new UserLoginUpdatedEvent(this, new TenantId(user.getTenant().getId()), userId, oldLogin, user.getLogin()));
    }

    @Transactional
    public void updateEmail(UserId userId, String email) throws EmailAlreadyExistsException, InvalidEmailException, UserNotFoundException {
        User user = getUser(userId);

        String oldEmail = user.getPerson().getContactInformation().getEmail();
        email = email.trim().toLowerCase();

        if (oldEmail.equals(email)) {
            return;
        }

        checkEmail(email);

        user.changeEmail(EmailValidor.validate(email));
        applicationEventPublisher.publishEvent(new UserEmailUpdatedEvent(this, userId, oldEmail, email));
    }

    @Transactional
    public User updateName(UserId userId, String firstName, String lastName) throws UserNotFoundException {
        User user = getUser(userId);

        String oldFirstName = user.getPerson().getFirstName();
        String oldLastName = user.getPerson().getLastName();

        if (oldFirstName.equals(firstName) && oldLastName.equals(lastName)) {
            return user;
        }

        user.setFirstName(firstName);
        user.setLastName(lastName);
        applicationEventPublisher.publishEvent(new UserDisplayNameUpdatedEvent(this, userId, firstName, lastName));
        return user;
    }

    @Transactional
    public User updateAvatar(UserId userId, String avatarId) throws UserNotFoundException {
        User user = getUser(userId);
        user.setAvatarId(avatarId);
        return user;
    }

    @Transactional
    public void forgotPassword(String loginOrEmail) throws UserDisabledException {
        Validate.notEmpty(loginOrEmail, "loginOrEmail is empty");
        User user = userRepository.findByLoginOrEmail(loginOrEmail);
        if (user == null) {
            return;
        }

        if (!user.isEnabled()) {
            throw new UserDisabledException(user.getLogin());
        }

        user.setPasswordToken();

        String email = user.getPerson().getContactInformation().getEmail();
        applicationEventPublisher.publishEvent(new ChangePasswordRequestedEvent(this, user.getPasswordToken(), email));
    }

    @Transactional
    public User cancelInvitation(UserId userId) throws UserNotFoundException {
        User user = getUser(userId);
        user.resetActivationToken();
        userRepository.delete(user);
        return user;
    }

    @Transactional
    public void deleteByTenantId(TenantId tenantId) {
        List<User> users = userRepository.findByTenantId(tenantId.getId());
        for (User user : users) {
            delete(user);
        }
    }

    private User getUser(UserId userId) throws UserNotFoundException {
        User user = userRepository.findOne(userId.getId());
        if (user == null) {
            throw new UserNotFoundException(userId);
        }
        return user;
    }

    private void checkEmail(String email) throws InvalidEmailException, EmailAlreadyExistsException {
        EmailValidor.validate(email);
        if (userRepository.findByEmail(email) != null) {
            throw new EmailAlreadyExistsException(email);
        }
    }
}
