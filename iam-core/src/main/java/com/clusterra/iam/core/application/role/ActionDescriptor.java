/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class ActionDescriptor {

    private final String actionId;
    private final String actionName;


    public ActionDescriptor(String actionId, String actionName) {
        this.actionId = actionId;
        this.actionName = actionName;
    }


    public String getActionId() {
        return actionId;
    }


    public String getActionName() {
        return actionName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActionDescriptor that = (ActionDescriptor) o;

        return actionId.equals(that.actionId) && actionName.equals(that.actionName);
    }


    @Override
    public int hashCode() {
        int result = actionId.hashCode();
        result = 31 * result + actionName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ActionDescriptor{" +
                "actionId=" + actionId +
                ", name='" + actionName + '\'' +
                '}';
    }
}
