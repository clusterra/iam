
    create table iam_action (
        id varchar(255) not null,
        name varchar(255),
        primary key (id)
    );

    create table iam_authorization (
        id varchar(255) not null,
        action_id varchar(255),
        role_id varchar(255),
        primary key (id)
    );

    create table iam_authorized_membership (
        id varchar(255) not null,
        group_id varchar(255),
        role_id varchar(255),
        user_id varchar(255),
        primary key (id)
    );

    create table iam_group (
        id varchar(255) not null,
        name varchar(255),
        tenant_id varchar(255),
        primary key (id)
    );

    create table iam_role (
        id varchar(255) not null,
        name varchar(255),
        tenant_id varchar(255),
        primary key (id)
    );

    create table iam_tenant (
        id varchar(255) not null,
        expiresAt timestamp,
        token varchar(255),
        avatarId varchar(255),
        createdByUserId varchar(255),
        createdDate timestamp,
        description varchar(255),
        language varchar(255),
        lastModifiedByUserId varchar(255),
        lastModifiedDate timestamp,
        location varchar(255),
        name varchar(255),
        signUpEmail varchar(255),
        status varchar(255),
        webSite varchar(255),
        primary key (id)
    );

    create table iam_user (
        id varchar(255) not null,
        activationTokenExpiresAt timestamp,
        activationToken varchar(255),
        createdByUserId varchar(255),
        createdDate timestamp,
        lastLoginDate timestamp,
        lastModifiedByUserId varchar(255),
        lastModifiedDate timestamp,
        login varchar(255),
        passwordHash varchar(255),
        passwordTokenExpiresAt timestamp,
        passwordToken varchar(255),
        avatarId varchar(255),
        email varchar(255),
        firstName varchar(255),
        lastName varchar(255),
        status varchar(255),
        tenant_id varchar(255),
        primary key (id)
    );

    alter table iam_action 
        add constraint UK_r7kl88nsn672r1519bxxgue3t  unique (name);

    alter table iam_authorization 
        add constraint UK_nettvdn8bcngqumgxams3s38j  unique (role_id, action_id);

    alter table iam_authorized_membership 
        add constraint UK_jqw5k1b2fiab8wnp4uo5xfxf3  unique (user_id, role_id, group_id);

    alter table iam_tenant 
        add constraint UK_2si9ex9gmtbsbjktjfh1v9pre  unique (token);

    alter table iam_tenant 
        add constraint UK_jpi6nd9w5hygg66sps73xkkcr  unique (name);

    alter table iam_tenant 
        add constraint UK_d2bp678lo588t88mtai3qhy45  unique (signUpEmail);

    alter table iam_user 
        add constraint UK_exsa8i4uktb8u34pom2w0gx94  unique (login);

    alter table iam_user 
        add constraint UK_b1bpb4m3sg3pe88kmg6mxfsbl  unique (email);

    alter table iam_authorization 
        add constraint FK_8utjj3f8pqndds213unvt20u8 
        foreign key (action_id) 
        references iam_action;

    alter table iam_authorization 
        add constraint FK_8lcmy80e4d1ewbw5wjs7j59fo 
        foreign key (role_id) 
        references iam_role;

    alter table iam_authorized_membership 
        add constraint FK_sanb51jkc37kw5hn5t18hj2xn 
        foreign key (group_id) 
        references iam_group;

    alter table iam_authorized_membership 
        add constraint FK_s6r9ivlmtomtqv35vv04dorqf 
        foreign key (role_id) 
        references iam_role;

    alter table iam_authorized_membership 
        add constraint FK_e3u1mkl0i90rva95b16ucc3jo 
        foreign key (user_id) 
        references iam_user;

    alter table iam_group 
        add constraint FK_fysnk62qgsauptyagqdvkkdfc 
        foreign key (tenant_id) 
        references iam_tenant;

    alter table iam_role 
        add constraint FK_qq1h1riduqjpjc3lf51c75b3u 
        foreign key (tenant_id) 
        references iam_tenant;

    alter table iam_user 
        add constraint FK_4uy49979m9lj41a6c8fbftxvw 
        foreign key (tenant_id) 
        references iam_tenant;
