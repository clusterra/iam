/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

import com.clusterra.iam.core.AbstractTenantTests;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class ActionServiceTest extends AbstractTenantTests {

    @Autowired
    private ActionService actionService;

    @Autowired
    private RoleService roleService;

    private ActionDescriptor action;
    private RoleDescriptor role;


    @BeforeMethod
    public void beforeMethod() throws Exception {

        String actionName = randomAlphabetic(5);
        action = actionService.createAction(actionName);
        String roleName = randomAlphabetic(5);
        role = roleService.createRole(tenantId, roleName);
    }

    @Test(expectedExceptions = ActionAlreadyExistsException.class)
    public void test_create_action() throws Exception {
        String name = randomAlphabetic(5);
        ActionDescriptor actionDescriptor = actionService.createAction(name);
        assertThat(actionService.findActionByName(actionDescriptor.getActionName()), notNullValue());
        actionService.createAction(name);
    }

    @Test(dependsOnMethods = "test_find_action_by_name")
    public void test_delete_action() {
        actionService.deleteAction(action);
        ActionDescriptor actionDescriptor = actionService.findActionByName(action.getActionName());
        assertThat(actionDescriptor, nullValue());
    }

    @Test
    public void test_find_action_by_name() {
        ActionDescriptor actionDescriptor = actionService.findActionByName(action.getActionName());
        assertThat(actionDescriptor, equalTo(action));
    }

    @Test
    public void test_find_action_by_name_throws_exception() {
        ActionDescriptor actionDescriptor = actionService.findActionByName(randomAlphabetic(5));
        assertThat(actionDescriptor, nullValue());
    }

    @Test
    public void test_find_all_actions() {
        Set<ActionDescriptor> actions = actionService.findAllActions();
        assertThat(actions, hasItem(action));
    }

    @Test
    public void test_allow_action_for_role() throws Exception {
        actionService.allowActionForRole(action, role);
        Set<ActionDescriptor> actions = actionService.findActionsByRole(role);
        assertThat(actions, contains(action));
    }

    @Test(expectedExceptions = ActionAlreadyAllowedException.class)
    public void test_allow_action_for_role_twice() throws Exception {
        actionService.allowActionForRole(action, role);
        actionService.allowActionForRole(action, role);
        Set<ActionDescriptor> actions = actionService.findActionsByRole(role);
        assertThat(actions, contains(action));
    }

    @Test
    public void test_is_action_allowed() throws Exception {
        boolean result = actionService.isActionAllowed(action, role);
        assertThat(result, is(false));
        actionService.allowActionForRole(action, role);
        result = actionService.isActionAllowed(action, role);
        assertThat(result, is(true));
    }

    @Test(dependsOnMethods = "test_allow_action_for_role")
    public void test_disallow_action_for_role() throws Exception {
        actionService.allowActionForRole(action, role);
        Set<ActionDescriptor> actions = actionService.findActionsByRole(role);
        assertThat(actions, contains(action));
        actionService.disallowActionForRole(action, role);
        actions = actionService.findActionsByRole(role);
        assertThat(actions, not(contains(action)));
    }

    @Test
    public void test_find_associated_roles() throws Exception {
        Set<RoleDescriptor> roles = actionService.findRolesByAction(action);
        assertThat(roles, not(contains(role)));
        actionService.allowActionForRole(action, role);

        roles = actionService.findRolesByAction(action);
        assertThat(roles, contains(role));
    }
}
