/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.AbstractTenantTests;
import com.clusterra.iam.core.application.user.UserId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class TenantQueryServiceTest extends AbstractTenantTests {

    @Autowired
    private TenantQueryService tenantQueryService;

    private UserId fakeUserId = new UserId(randomAlphanumeric(10));
    private TenantId fakeTenantId = new TenantId(randomAlphabetic(10));

    @Test
    public void test_is_tenant_name_taken() {

        boolean taken = tenantQueryService.isNameTaken(tenant.getName());
        assertThat(taken, is(true));

        taken = tenantQueryService.isNameTaken(randomAlphabetic(3));
        assertThat(taken, is(false));
    }

    @Test
    public void test_find_tenant_by_user() throws Exception {
        Tenant tenant = tenantQueryService.findByUser(userId);
        assertThat(tenant, notNullValue());
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_find_tenant_by_user_throws_exception() throws Exception {
        tenantQueryService.findByUser(fakeUserId);
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_find_tenant_by_id_throws_exception() throws Exception {
        tenantQueryService.find(fakeTenantId);
    }

    @Test
    public void test_find_tenant_by_name() throws Exception {
        String tenantName = tenantQueryService.find(tenantId).getName();
        Tenant t = tenantQueryService.findByName(tenantName);
        assertThat(t, notNullValue());
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_find_tenant_by_name_throws_exception() throws Exception {
        tenantQueryService.findByName(randomAlphabetic(3));
    }

    @Test
    public void test_created_tenant_is_enabled() throws Exception {
        assertThat(tenantQueryService.isEnabled(tenantId), is(true));
    }

    @Test
    public void test_find_all() throws Exception {
        createRandomTenant();
        createRandomTenant();
        Tenant tenant = tenantQueryService.find(this.tenantId);
        String name = tenant.getName();
        Page<Tenant> page = tenantQueryService.findAll(new PageRequest(0, 10), name);
        assertThat(page.getContent(), hasSize(1));
        page = tenantQueryService.findAll(new PageRequest(0, 10), "");
        assertThat(page.getContent().size(), greaterThan(1));
    }

    @Test
    public void test_total_count() throws Exception {
        assertThat(tenantQueryService.getTotalCount(), greaterThan(0));
    }
}
