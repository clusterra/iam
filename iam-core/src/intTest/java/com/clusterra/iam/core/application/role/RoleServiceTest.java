/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

import com.clusterra.iam.core.AbstractTenantTests;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Set;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class RoleServiceTest extends AbstractTenantTests {

    @Autowired
    private RoleService roleService;


    private RoleDescriptor role;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        role = roleService.createRole(tenantId, randomAlphabetic(5));
    }

    @Test
    public void test_find_role_by_name() throws Exception {
        RoleDescriptor roleDescriptor = roleService.findRoleByName(tenantId, role.getRoleName());
        assertThat(roleDescriptor, equalTo(role));
    }

    @Test
    public void test_find_or_create_role_by_name() throws Exception {
        RoleDescriptor roleDescriptor = roleService.findOrCreateRole(tenantId, role.getRoleName());
        assertThat(roleDescriptor, equalTo(role));
        roleDescriptor = roleService.findOrCreateRole(tenantId, randomAlphabetic(5));
        assertThat(roleDescriptor, not(equalTo(role)));
    }


    @Test
    public void test_find_all_roles() {
        Set<RoleDescriptor> roles = roleService.findAllRoles(tenantId);
        assertThat(roles, hasItem(role));
    }

    @Test(expectedExceptions = RoleNotFoundException.class)
    public void test_delete_role() throws Exception {
        RoleDescriptor roleDescriptor = roleService.findRoleByName(tenantId, role.getRoleName());
        assertThat(roleDescriptor, equalTo(role));
        roleService.deleteRole(role);
        roleService.findRoleByName(tenantId, this.role.getRoleName());
    }

}
