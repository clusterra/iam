/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.tenant;

import com.clusterra.iam.core.AbstractTenantTests;
import com.clusterra.iam.core.application.tenant.event.TenantSignedUpEvent;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.tenant.TenantStatus;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class TenantCommandServiceTest extends AbstractTenantTests {

    @Autowired
    private TenantCommandService tenantCommandService;

    @Autowired
    private TenantQueryService tenantQueryService;

    @Autowired
    private UserCommandService userCommandService;

    private TenantId fakeTenantId = new TenantId(randomAlphabetic(10));

    @BeforeTest
    public void before() {
        System.setProperty("clusterra.iam-core.tenant-sign-up-timeout-days", "0");
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_delete_tenant() throws Exception {
        String tenantName = tenant.getName();
        Tenant t = tenantQueryService.findByName(tenantName);
        assertThat(t, equalTo(tenant));

        userCommandService.delete(userId);
        tenantCommandService.delete(tenantId);
        tenantQueryService.findByName(tenantName);
    }

    @Test(expectedExceptions = TenantAlreadyExistsException.class)
    public void test_create_tenant_throws_exception() throws Exception {
        String tenantName = tenantQueryService.find(tenantId).getName();
        tenantCommandService.create(tenantName, UserRandomUtil.getRandomEmail());
    }

    @Test
    public void test_enable_disable() throws Exception {
        assertThat(tenantQueryService.isEnabled(tenantId), is(true));

        tenantCommandService.disable(tenantId);
        assertThat(tenantQueryService.isEnabled(tenantId), is(false));

        tenantCommandService.enable(tenantId);
        assertThat(tenantQueryService.isEnabled(tenantId), is(true));
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_enable_throws_exception() throws Exception {
        tenantCommandService.enable(fakeTenantId);
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_disable_throws_exception() throws Exception {
        tenantCommandService.disable(fakeTenantId);
    }

    @Test
    public void test_enable_disable_is_idempotent() throws Exception {
        assertThat(tenantQueryService.isEnabled(tenantId), is(true));

        tenantCommandService.disable(tenantId);
        assertThat(tenantQueryService.isEnabled(tenantId), is(false));
        tenantCommandService.disable(tenantId);
        assertThat(tenantQueryService.isEnabled(tenantId), is(false));

        tenantCommandService.enable(tenantId);
        assertThat(tenantQueryService.isEnabled(tenantId), is(true));
        tenantCommandService.enable(tenantId);
        assertThat(tenantQueryService.isEnabled(tenantId), is(true));
    }

    @Test
    public void test_update_tenant() throws Exception {
        assertThat(tenant, notNullValue());

        Tenant tenant = tenantQueryService.find(this.tenantId);
        String name = tenant.getName();
        String description = tenant.getDescription();
        String language = tenant.getLanguage();
        String location = tenant.getLocation();
        String webSite = tenant.getWebSite();
        assertThat(name, notNullValue());
        assertThat(description, nullValue());
        assertThat(language, nullValue());
        assertThat(location, nullValue());
        assertThat(webSite, nullValue());

        tenantCommandService.update(this.tenantId, randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5));

        tenant = tenantQueryService.find(this.tenantId);
        assertThat(tenant.getName(), not(equalTo(name)));
        assertThat(tenant.getDescription(), not(equalTo(description)));
        assertThat(tenant.getLanguage(), not(equalTo(language)));
        assertThat(tenant.getLocation(), not(equalTo(location)));
        assertThat(tenant.getWebSite(), not(equalTo(webSite)));
    }

    @Test(expectedExceptions = TenantAlreadyExistsException.class)
    public void test_update_tenant_name_throws_exception_1() throws Exception {
        String name = randomAlphabetic(5);
        tenantCommandService.create(name, UserRandomUtil.getRandomEmail());

        String newName = name;
        tenantCommandService.update(tenantId, newName, "", "", "", "");
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_update_tenant_name_throws_exception_2() throws Exception {
        String newName = randomAlphabetic(100);
        tenantCommandService.update(fakeTenantId, newName, "", "", "", "");
    }

    @Test
    public void test_activate() throws Exception {
        TenantId tenantId = new TenantId(signUpRandomTenant().getId());
        Tenant tenant = tenantQueryService.find(tenantId);
        assertThat(tenant, notNullValue());
        assertThat(tenant.getStatus(), equalTo(TenantStatus.SIGNED_UP));

        assertThat(tenantQueryService.isEnabled(tenantId), is(false));

        String token = getLastTenantSignedUpEvent().getActivationToken().getToken();
        tenantCommandService.activate(token, randomAlphabetic(5), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));

        tenant = tenantQueryService.find(tenantId);
        assertThat(tenant.getStatus(), equalTo(TenantStatus.ENABLED));

        assertThat(tenantQueryService.isEnabled(tenantId), is(true));
    }

    @Test
    public void test_purge_expired() throws Exception {

        signUpRandomTenant();
        signUpRandomTenant();
        signUpRandomTenant();
        signUpRandomTenant();
        signUpRandomTenant();

        Integer before = tenantQueryService.getTotalCount();
        Integer purged = tenantCommandService.purgeExpiredSignUps();
        Integer after = tenantQueryService.getTotalCount();
        assertThat(purged, greaterThan(0));
        assertThat(before, greaterThan(after));
    }

    @Test(expectedExceptions = InvalidTenantActivationTokenException.class)
    public void test_activate_throws_exception() throws Exception {
        tenantCommandService.activate(randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5));
    }

    @Test(enabled = false, expectedExceptions = InvalidTenantActivationTokenException.class)
    public void test_activation_token_expires() throws Exception {
        TenantId randomTenant = new TenantId(signUpRandomTenant().getId());
        TenantSignedUpEvent event = getLastTenantSignedUpEvent();
        assertThat(randomTenant, equalTo(event.getTenantId()));
        tenantCommandService.activate(event.getActivationToken().getToken(), randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(5));
    }
}
