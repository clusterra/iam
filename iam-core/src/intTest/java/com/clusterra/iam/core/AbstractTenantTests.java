/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.event.TenantSignedUpEvent;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.application.user.LoginAlreadyExistsException;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.application.user.event.ChangePasswordRequestedEvent;
import com.clusterra.iam.core.application.user.event.UserInvitedEvent;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import com.clusterra.iam.core.util.ChangePasswordRequestedEventInterceptor;
import com.clusterra.iam.core.util.TenantSignedUpEventInterceptor;
import com.clusterra.iam.core.util.UserInvitedEventInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class
)
public abstract class AbstractTenantTests extends AbstractTestNGSpringContextTests {

    @Autowired
    protected TenantCommandService tenantCommandService;

    @Autowired
    protected UserCommandService userCommandService;

    @Autowired
    protected IdentityTrackerLifeCycle identityTrackerLifeCycle;

    @Autowired
    protected TenantSignedUpEventInterceptor tenantSignedUpEventInterceptor;

    @Autowired
    protected UserInvitedEventInterceptor userInvitedEventInterceptor;

    @Autowired
    protected ChangePasswordRequestedEventInterceptor changePasswordRequestedEventInterceptor;

    @Autowired
    protected RoleService roleService;

    @Autowired
    protected AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    protected GroupService groupService;

    protected TenantId tenantId;
    protected UserId userId;
    protected UserId adminUserId;
    protected Tenant tenant;

    @BeforeMethod
    public void create_tenant() throws Exception {

        //TODO this is used in listeners for sending email.
        //TODO make it better some time
//        MockHttpServletRequest request = new MockHttpServletRequest();
//        request.addHeader("host", "server-host");
//        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request, new MockHttpServletResponse()));

        tenantSignedUpEventInterceptor.clear();
        changePasswordRequestedEventInterceptor.clear();
        userInvitedEventInterceptor.clear();
        tenant = createRandomTenant();
        tenantId = new TenantId(tenant.getId());

        userId = new UserId(createRandomUser(tenantId).getId());

        RoleDescriptor role = roleService.createRole(tenantId, DefaultRole.ADMIN);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));
        adminUserId = new UserId(createRandomUser(tenantId).getId());

        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, adminUserId, role, group);

        identityTrackerLifeCycle.startTracking(adminUserId, tenantId);

    }

    protected User createRandomUser(TenantId tenantId) throws EmailAlreadyExistsException, InvalidEmailException, LoginAlreadyExistsException, TenantNotFoundException {
        return userCommandService.create(tenantId, randomAlphabetic(10), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
    }

    protected Tenant createRandomTenant() throws TenantAlreadyExistsException, InvalidTenantNameException, InvalidEmailException {
        return tenantCommandService.create(randomAlphabetic(10), UserRandomUtil.getRandomEmail());
    }

    protected Tenant signUpRandomTenant() throws TenantAlreadyExistsException, InvalidEmailException, EmailAlreadyExistsException, InvalidTenantNameException {
        String tenantName = randomAlphabetic(10);
        return tenantCommandService.signUp(tenantName, UserRandomUtil.getRandomEmail());
    }

    protected TenantSignedUpEvent getLastTenantSignedUpEvent() {
        return tenantSignedUpEventInterceptor.getLastEvent();
    }

    protected User inviteRandomUser(TenantId tenantId) throws EmailAlreadyExistsException, InvalidEmailException, TenantNotFoundException {
        return userCommandService.invite(tenantId, UserRandomUtil.getRandomEmail());
    }

    protected UserInvitedEvent getLastUserInvitedEvent() {
        return userInvitedEventInterceptor.getLastEvent();
    }

    protected ChangePasswordRequestedEvent getLastChangePasswordRequestedEvent() {
        return changePasswordRequestedEventInterceptor.getLastEvent();
    }
}
