/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core;

import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tracker.IdentityTrackerLifeCycle;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.application.user.LoginAlreadyExistsException;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 03.12.13
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class
)
public abstract class AbstractTenantTransactionalTests extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    protected TenantCommandService tenantCommandService;

    @Autowired
    protected UserCommandService userCommandService;

    @Autowired
    private IdentityTrackerLifeCycle identityTrackerLifeCycle;

    protected TenantId tenantId;
    protected UserId userId;

    @BeforeMethod
    public void create_tenant() throws Exception {
        tenantId = new TenantId(createRandomTenant().getId());
        userId = new UserId(createRandomUser().getId());
        identityTrackerLifeCycle.startTracking(userId, tenantId);

    }

    protected User createRandomUser() throws EmailAlreadyExistsException, InvalidEmailException, LoginAlreadyExistsException, TenantNotFoundException {
        return userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
    }

    protected Tenant createRandomTenant() throws TenantAlreadyExistsException, InvalidTenantNameException, InvalidEmailException {
        return tenantCommandService.create(randomAlphabetic(5), UserRandomUtil.getRandomEmail());
    }


}
