/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user;

import com.clusterra.iam.core.AbstractTenantTests;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 09.08.13 : 22:47
 */
public class UserCommandServiceTest extends AbstractTenantTests {

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private UserQueryService userQueryService;


    private TenantId fakeTenant = new TenantId(randomAlphabetic(10));
    private UserId fakeUser = new UserId(randomAlphanumeric(10));
    private String login;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        login = userQueryService.findUser(userId).getLogin();
    }

    @Test
    public void test_user_name_in_cyrillic() throws Exception {
        String firstName = "Иван";
        String lastName = "ИвановЁё";
        User user = userCommandService.create(tenantId, randomAlphabetic(10), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), firstName, lastName);
        assertThat(firstName, equalTo(user.getPerson().getFirstName()));
        assertThat(lastName, equalTo(user.getPerson().getLastName()));
    }

    @Test
    public void test_user_name_with_numbers() throws Exception {
        String firstName = "Иван12";
        String lastName = "ИвановЁё11";
        User user = userCommandService.create(tenantId, randomAlphabetic(10), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), firstName, lastName);
        assertThat(firstName, equalTo(user.getPerson().getFirstName()));
        assertThat(lastName, equalTo(user.getPerson().getLastName()));
    }

    @Test(expectedExceptions = LoginAlreadyExistsException.class)
    public void test_create_user_throws_exception_1() throws Exception {
        userCommandService.create(tenantId, login, UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), "", "");
    }

    @Test(expectedExceptions = EmailAlreadyExistsException.class)
    public void test_create_user_throws_exception_2() throws Exception {
        String email = UserRandomUtil.getRandomEmail();
        userCommandService.create(tenantId, randomAlphabetic(5), email, UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        userCommandService.create(tenantId, randomAlphabetic(5), email, UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
    }

    @Test(expectedExceptions = TenantNotFoundException.class)
    public void test_create_user_throws_exception_3() throws Exception {
        createRandomUser(fakeTenant);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void test_create_user_throws_exception_4() throws Exception {
        userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), "", "");
    }

    @Test
    public void test_invite_user_not_enabled() throws Exception {
        inviteRandomUser(tenantId);

        String activationToken = getLastUserInvitedEvent().getActivationToken();

        User user = userQueryService.findByActivationToken(activationToken);
        assertThat(user.isEnabled(), is(false));
    }

    @Test
    public void test_enable_disable__does_not_affect_activation() throws Exception {
        inviteRandomUser(tenantId);

        String activationToken = getLastUserInvitedEvent().getActivationToken();
        userQueryService.checkActivationToken(activationToken);

        User user = userQueryService.findByActivationToken(activationToken);
        user = userCommandService.enable(new UserId(user.getId()));

        assertThat(user.isEnabled(), is(true));
        userQueryService.checkActivationToken(activationToken);
        user = userCommandService.disable(new UserId(user.getId()));
        assertThat(user.isEnabled(), is(false));
        userQueryService.checkActivationToken(activationToken);
    }

    @Test(expectedExceptions = UserNotFoundException.class)
    public void test_enable_throws_exception() throws Exception {
        userCommandService.enable(fakeUser);
    }

    @Test
    public void test_invite_user_and_enable() throws Exception {
        inviteRandomUser(tenantId);

        String activationToken = getLastUserInvitedEvent().getActivationToken();

        User user = userQueryService.findByActivationToken(activationToken);
        assertThat(user.isEnabled(), is(false));
        user = userCommandService.enable(new UserId(user.getId()));
        assertThat(user.isEnabled(), is(true));
    }

    @Test(expectedExceptions = UserDisabledException.class)
    public void test_reset_password_for_newly_invited_user_throws_exception() throws Exception {
        inviteRandomUser(tenantId);
        String activationToken = getLastUserInvitedEvent().getActivationToken();

        User user = userQueryService.findByActivationToken(activationToken);
        userCommandService.forgotPassword(user.getPerson().getContactInformation().getEmail());
    }

    @Test
    public void test_activate_user() throws Exception {
        inviteRandomUser(tenantId);

        String activationToken = getLastUserInvitedEvent().getActivationToken();

        User user = userCommandService.activate(activationToken, randomAlphabetic(5), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        assertThat(user.isEnabled(), is(true));
    }


    @Test(expectedExceptions = UserNotFoundException.class)
    public void test_delete_user() throws Exception {
        assertThat(userQueryService.findUser(userId), notNullValue());
        userCommandService.delete(userId);
        userQueryService.findUser(userId);
    }

    @Test
    public void test_activate_then_disable_user() throws Exception {
        inviteRandomUser(tenantId);

        String activationToken = getLastUserInvitedEvent().getActivationToken();

        User user = userCommandService.activate(activationToken, randomAlphabetic(5), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        assertThat(user.isEnabled(), is(true));

        user = userCommandService.disable(new UserId(user.getId()));
        assertThat(user.isEnabled(), is(false));
    }

    @Test
    public void test_update_login() throws Exception {
        String login = randomAlphabetic(5);
        userCommandService.updateLogin(userId, login);
        String login1 = userQueryService.findUser(userId).getLogin();
        assertThat(login1, equalToIgnoringCase(login));
    }

    @Test
    public void test_update_name() throws Exception {
        String rodriguez = " Rodriguez";
        userCommandService.updateName(userId, "Bender", rodriguez);
        String lastName = userQueryService.findUser(userId).getPerson().getLastName();
        assertThat(lastName, equalTo(rodriguez.trim()));
    }

    @Test(expectedExceptions = UserNotFoundException.class)
    public void test_update_login_throws_exception_1() throws Exception {
        String login = randomAlphabetic(5);
        userCommandService.updateLogin(fakeUser, login);
    }

    @Test(expectedExceptions = LoginAlreadyExistsException.class)
    public void test_update_login_throws_exception_2() throws Exception {
        String login = randomAlphabetic(5);
        userCommandService.create(tenantId, login, UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        userCommandService.updateLogin(userId, login);
    }

    @Test(expectedExceptions = UserNotFoundException.class)
    public void test_update_email_throws_exception_1() throws Exception {
        String email = UserRandomUtil.getRandomEmail();
        userCommandService.updateEmail(fakeUser, email);
    }

    @Test(expectedExceptions = EmailAlreadyExistsException.class)
    public void test_update_email_throws_exception_2() throws Exception {
        String email = UserRandomUtil.getRandomEmail();

        userCommandService.create(tenantId, email, email, UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        userCommandService.updateEmail(userId, email);
    }

    @Test(expectedExceptions = InvalidEmailException.class)
    public void test_update_email_throws_exception_3() throws Exception {
        String wrong = randomAlphabetic(3);
        userCommandService.updateEmail(userId, wrong);
    }


    @Test(expectedExceptions = InvalidPasswordTokenException.class)
    public void test_change_password() throws Exception {

        userCommandService.forgotPassword(login);
        String passwordToken = getLastChangePasswordRequestedEvent().getPasswordToken().getToken();

        String newPassword = "new" + UserRandomUtil.randomPassword();
        userCommandService.resetPassword(passwordToken, newPassword);

        userQueryService.findByPasswordToken(passwordToken);
    }

    @Test(expectedExceptions = UserDisabledException.class)
    public void test_change_password_for_disabled_user_throws_exception() throws Exception {

        userCommandService.forgotPassword(login);
        String passwordToken = getLastChangePasswordRequestedEvent().getPasswordToken().getToken();

        String newPassword = "new" + UserRandomUtil.randomPassword();
        userCommandService.disable(userId);
        userCommandService.resetPassword(passwordToken, newPassword);
    }

    @Test
    public void test_reset_password() throws Exception {
        User user = userQueryService.findUser(userId);
        assertThat(user.isEnabled(), is(true));
        userCommandService.forgotPassword(login);
        user = userQueryService.findUser(userId);
        assertThat(user.isEnabled(), is(true));
        assertThat(getLastChangePasswordRequestedEvent(), notNullValue());
    }

    @Test(expectedExceptions = UserDisabledException.class)
    public void test_reset_password_for_disabled_user_throws_exception() throws Exception {
        userCommandService.disable(userId);
        userCommandService.forgotPassword(login);
    }


}
