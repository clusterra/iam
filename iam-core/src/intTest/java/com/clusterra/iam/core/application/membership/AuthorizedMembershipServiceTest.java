/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.membership;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.AbstractTenantTests;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.12.13
 */
public class AuthorizedMembershipServiceTest extends AbstractTenantTests {

    @Autowired
    private RoleService roleService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    private TenantId wrongTenantId;
    private RoleDescriptor role1;
    private RoleDescriptor role2;
    private UserId userId_1;
    private UserId userId_2;
    private GroupDescriptor group1;
    private GroupDescriptor group2;


    @BeforeMethod
    public void beforeMethod() throws Exception {
        wrongTenantId = new TenantId(createRandomTenant().getId());
        String email1 = UserRandomUtil.getRandomEmail();
        User user_1 = userCommandService.create(tenantId, email1, email1, UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        userId_1 = new UserId(user_1.getId());
        String email2 = UserRandomUtil.getRandomEmail();
        User user_2 = userCommandService.create(tenantId, email2, email2, UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        userId_2 = new UserId(user_2.getId());
        role1 = roleService.createRole(tenantId, randomAlphabetic(5));
        role2 = roleService.createRole(tenantId, randomAlphabetic(5));

        group1 = groupService.createGroup(tenantId, randomAlphabetic(5));
        group2 = groupService.createGroup(tenantId, randomAlphabetic(5));
    }

    @Test
    public void test_find_users_of_group() throws Exception {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role1, group1);

        List<UserId> users = authorizedMembershipService.findUsersByGroup(group1);
        assertThat(users, hasSize(1));

        GroupDescriptor group2 = groupService.createGroup(tenantId, randomAlphabetic(5));

        users = authorizedMembershipService.findUsersByGroup(group2);
        assertThat(users, hasSize(0));
    }

    @Test
    public void testRevokeRole() throws Exception {

    }

    @Test
    public void test_assign_role() {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role1, group1);
        List<UserId> users = authorizedMembershipService.findUsersByGroupAndRole(group1, role1);
        assertThat(users, contains(userId_1));
        assertThat(users, not(contains(userId_2)));
    }

    @Test(expectedExceptions = WrongTenantException.class)
    public void test_assign_role_wrong_tenant() {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(wrongTenantId, userId_1, role1, group1);
    }

    @Test
    public void test_revoke_role() {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role1, group1);
        List<UserId> users = authorizedMembershipService.findUsersByGroupAndRole(group1, role1);
        assertThat(users, contains(userId_1));
        assertThat(users, not(contains(userId_2)));
        authorizedMembershipService.deleteAuthorizedMembership(tenantId, userId_1, role1, group1);
        users = authorizedMembershipService.findUsersByGroupAndRole(group1, role1);
        assertThat(users, not(contains(userId_1)));
        assertThat(users, not(contains(userId_2)));
    }

    @Test(expectedExceptions = WrongTenantException.class)
    public void test_revoke_role_wrong_tenant() {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role1, group1);
        List<UserId> users = authorizedMembershipService.findUsersByGroupAndRole(group1, role1);
        assertThat(users, contains(userId_1));
        assertThat(users, not(contains(userId_2)));
        authorizedMembershipService.deleteAuthorizedMembership(wrongTenantId, userId_1, role1, group1);
    }

    @Test
    public void test_find_users_by_group_and_role() {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role1, group1);
        List<UserId> users = authorizedMembershipService.findUsersByGroupAndRole(group1, role1);
        assertThat(users, contains(userId_1));
        assertThat(users, not(contains(userId_2)));
    }

    @Test
    public void test_find_roles_by_user_and_group() {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role1, group1);
        List<RoleDescriptor> roles = authorizedMembershipService.findRolesByGroupAndUser(group1, userId_1);
        assertThat(roles, contains(role1));
    }

    @Test
    public void test_find_roles_by_user() {
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role1, group1);
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId_1, role2, group2);
        List<RoleDescriptor> roles = authorizedMembershipService.findRolesByUser(userId_1);
        assertThat(roles, hasSize(2));
        assertThat(roles, hasItem(role1));
        assertThat(roles, hasItem(role2));
    }
}
