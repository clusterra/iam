/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.user;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.AbstractTenantTests;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mapping.PropertyReferenceException;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.testng.Assert.fail;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 19.12.13
 */

public class UserQueryServiceTest extends AbstractTenantTests {

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private UserQueryService userQueryService;

    private TenantId fakeTenant = new TenantId(randomAlphabetic(10));


    @Test
    public void test_find_all_users() throws Exception {
        for (int i = 0; i < 5; i++) {
            createRandomUser(tenantId);
        }
        Page<User> page = userQueryService.findAll(tenantId, new PageRequest(0, 3), "");
        assertThat(page.getContent(), hasSize(3));
    }

    @Test
    public void test_find_all_users_search_by() throws Exception {
        for (int i = 0; i < 4; i++) {
            createRandomUser(tenantId);
        }
        userCommandService.create(tenantId, "Abc" + randomAlphabetic(5), "Abc" + UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), "Abc" + randomAlphabetic(5), "Abc" + randomAlphabetic(5));
        userCommandService.create(tenantId, "aBc" + randomAlphabetic(5), "aBc" + UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), "aBc" + randomAlphabetic(5), "aBc" + randomAlphabetic(5));

        Page<User> page = userQueryService.findAll(tenantId, new PageRequest(0, 100), "ABc");
        assertThat(page.getContent(), hasSize(2));
        assertThat(page.getTotalElements(), equalTo(2l));
    }

    @Test
    public void test_update_email() throws Exception {
        String email = UserRandomUtil.getRandomEmail();
        userCommandService.updateEmail(userId, email);
        assertThat(userQueryService.findUser(userId).getPerson().getContactInformation().getEmail(), equalToIgnoringCase(email));
    }

    @Test
    public void test_update_email_same_value() throws Exception {
        String email = UserRandomUtil.getRandomEmail();
        userCommandService.updateEmail(userId, email);
        assertThat(userQueryService.findUser(userId).getPerson().getContactInformation().getEmail(), equalToIgnoringCase(email));
    }

    @Test
    public void test_update_first_last_name() throws Exception {
        String firstName = randomAlphabetic(5);
        String lastName = randomAlphabetic(5);

        String oldName = userQueryService.findUser(userId).getPerson().getDisplayName();
        userCommandService.updateName(userId, firstName, lastName);
        assertThat(oldName, not(equalTo(userQueryService.findUser(userId).getPerson().getDisplayName())));
    }

    @Test
    public void test_find_by_like() throws Exception {
        String prefix = randomAlphabetic(10);
        userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), prefix + randomAlphabetic(5), randomAlphabetic(5));
        userCommandService.create(tenantId, randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5) + prefix);

        Page<User> users = userQueryService.findAll(tenantId, new PageRequest(0, 100), prefix);
        assertThat(users.getContent(), hasSize(2));
    }

    @Test
    public void test_get_total_users_count() {
        Integer count = userQueryService.getTotalCount(tenantId);
        assertThat(count, greaterThan(0));

        count = userQueryService.getTotalCount(fakeTenant);
        assertThat(count, equalTo(0));
    }

    @Test
    public void test_get_total_activated_users_count() throws Exception {
        createRandomUser(tenantId);

        Page<User> page = userQueryService.findActivated(tenantId, new PageRequest(0, 1), "");
        assertThat(page.getContent(), hasSize(1));
        assertThat(page.getTotalElements(), greaterThan(1l));

    }

    @Test
    public void test_sorting_columns() throws Exception {
        createRandomUser(tenantId);

        Page<User> page = userQueryService.findActivated(tenantId, new PageRequest(0, 1), "");
        assertThat(page.getContent(), hasSize(1));
        page = userQueryService.findActivated(tenantId, new PageRequest(0, 1, Sort.Direction.DESC, "person.firstName"), "");
        assertThat(page.getContent(), hasSize(1));
        page = userQueryService.findActivated(tenantId, new PageRequest(0, 1, Sort.Direction.DESC, "person.lastName"), "");
        assertThat(page.getContent(), hasSize(1));
        page = userQueryService.findActivated(tenantId, new PageRequest(0, 1, Sort.Direction.DESC, "person.contactInformation.email"), "");
        assertThat(page.getContent(), hasSize(1));
        page = userQueryService.findActivated(tenantId, new PageRequest(0, 1, Sort.Direction.DESC, "status"), "");
        assertThat(page.getContent(), hasSize(1));
    }

    @Test
    public void test_sorting_columns_or_direction_throws_exception() throws Exception {
        createRandomUser(tenantId);

        try {
            userQueryService.findActivated(tenantId, new PageRequest(0, 1, Sort.Direction.DESC, "bla"), "");
            fail("expecting IAE");
        } catch (PropertyReferenceException e) {
        }
        try {
            userQueryService.findActivated(tenantId, new PageRequest(0, 1, Sort.Direction.DESC), "");
            fail();
        } catch (IllegalArgumentException e) {

        }
        try {
            userQueryService.findActivated(tenantId, new PageRequest(0, 1, Sort.Direction.DESC, "userId", "bla"), "");
            fail();
        } catch (PropertyReferenceException e) {

        }
    }

    @Test
    public void test_get_total_invited_users_count() throws Exception {
        inviteRandomUser(tenantId);
        inviteRandomUser(tenantId);

        Page<User> page = userQueryService.findInvited(tenantId, new PageRequest(0, 1), null);
        assertThat(page.getContent(), hasSize(1));
        assertThat(page.getTotalElements(), equalTo(2l));
    }
}
