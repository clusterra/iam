/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.crypto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
@SpringApplicationConfiguration(locations = {
        "classpath*:META-INF/spring/*.xml"
})
public class EncryptionServiceTest extends AbstractTestNGSpringContextTests {


    @Autowired
    private EncryptionService encryptionService;


    @Test
    public void test_hashed_password_match() {
        String password = "pAssw0rd";
        String hashed = encryptionService.encrypt(password);

        assertThat(encryptionService.matches(password, hashed), is(true));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void test_encrypt_throws_exception_for_weak_pasword() {
        String password = "pwd";
        encryptionService.encrypt(password);
    }


    @Test
    public void test_hashed_password_not_match() {
        String password1 = "pAssw0rd";
        String password2 = "pAssw0rda";
        String hashed = encryptionService.encrypt(password1);

        assertThat(encryptionService.matches(password2, hashed), is(false));
    }
}
