/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.group;

import com.clusterra.iam.core.AbstractTenantTests;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;


/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 26.11.13
 */
public class GroupServiceTest extends AbstractTenantTests {

    @Autowired
    private GroupService groupService;

    private GroupDescriptor groupDescriptor;

    @BeforeMethod
    private void beforeMethod() throws Exception {
        groupDescriptor = groupService.createGroup(tenantId, randomAlphabetic(5));
    }

    @Test
    public void test_find_group_by_name() throws Exception {
        GroupDescriptor groupByName = groupService.findGroupByName(tenantId, groupDescriptor.getGroupName());
        assertThat(groupDescriptor, equalTo(groupByName));
    }

    @Test(expectedExceptions = GroupNotFoundException.class)
    public void test_find_group_by_name_throws_exception() throws Exception {
        groupService.findGroupByName(tenantId, randomAlphabetic(5));
    }

    @Test
    public void test_is_group_name_taken() throws Exception {
        assertThat(groupService.isGroupNameTaken(tenantId, groupDescriptor.getGroupName()), is(true));
        assertThat(groupService.isGroupNameTaken(tenantId, randomAlphabetic(5)), is(false));
    }

    @Test(expectedExceptions = GroupAlreadyExistsException.class)
    public void test_create_group_throws_exception() throws Exception {
        groupService.createGroup(tenantId, groupDescriptor.getGroupName());
    }
}
