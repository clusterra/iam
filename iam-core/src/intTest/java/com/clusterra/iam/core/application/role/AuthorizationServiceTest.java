/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.application.role;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.AbstractTenantTests;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class AuthorizationServiceTest extends AbstractTenantTests {

    @Autowired
    private AuthorizationService authorizationService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ActionService actionService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    private ActionDescriptor action;


    @BeforeMethod
    public void beforeMethod() throws Exception {
        String actionName = randomAlphabetic(5);
        RoleDescriptor role = roleService.createRole(tenantId, randomAlphabetic(5));
        action = actionService.createAction(actionName);
        actionService.allowActionForRole(action, role);
        GroupDescriptor group = groupService.createGroup(tenantId, randomAlphabetic(5));
        authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, role, group);
    }

    @Test
    public void test_can_execute_action() {
        boolean b = authorizationService.canExecute(userId, action);
        assertThat(b, is(true));
    }
}
