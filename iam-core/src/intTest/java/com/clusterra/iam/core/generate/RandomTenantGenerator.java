/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.generate;

import com.clusterra.iam.core.application.tenant.InvalidTenantActivationTokenException;
import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 19.12.13
 */
public interface RandomTenantGenerator {

    public void generate() throws EmailAlreadyExistsException, TenantAlreadyExistsException, InvalidEmailException, InvalidTenantActivationTokenException, TenantNotFoundException, InvalidTenantNameException;

}
