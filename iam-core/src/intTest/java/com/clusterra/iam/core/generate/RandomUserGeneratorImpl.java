/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.generate;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.application.user.LoginAlreadyExistsException;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 19.12.13
 */
@Component
public class RandomUserGeneratorImpl implements RandomUserGenerator {

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private TenantQueryService tenantQueryService;

    @Autowired
    private UserQueryService userQueryService;

    public void generate(String tenantName) throws EmailAlreadyExistsException, LoginAlreadyExistsException, InvalidEmailException, TenantNotFoundException {

        Tenant tenant = tenantQueryService.findByName(tenantName);

        if (userQueryService.getTotalCount(new TenantId(tenant.getId())) > 100) {
            return;
        }

        for (int i = 0; i < 100; i++) {
            userCommandService.create(new TenantId(tenant.getId()), randomAlphabetic(5), UserRandomUtil.getRandomEmail(), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        }
    }
}
