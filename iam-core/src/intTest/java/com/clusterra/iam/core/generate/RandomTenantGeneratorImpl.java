/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.generate;

import com.clusterra.iam.core.application.tenant.InvalidTenantActivationTokenException;
import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import com.clusterra.iam.core.application.tenant.event.TenantSignedUpEvent;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.util.TenantSignedUpEventInterceptor;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 04.01.14
 */
@Service
public class RandomTenantGeneratorImpl implements RandomTenantGenerator {

    @Autowired
    private TenantSignedUpEventInterceptor tenantSignedUpEventInterceptor;

    @Autowired
    private TenantCommandService tenantCommandService;

    @Autowired
    private TenantQueryService tenantQueryService;


    public void generate() throws EmailAlreadyExistsException, TenantAlreadyExistsException, InvalidEmailException, InvalidTenantActivationTokenException, TenantNotFoundException, InvalidTenantNameException {

        if (tenantQueryService.getTotalCount() > 100) {
            return;
        }

        for (int i = 0; i < 100; i++) {
            tenantCommandService.signUp(randomAlphabetic(5), UserRandomUtil.getRandomEmail());
            TenantSignedUpEvent event = tenantSignedUpEventInterceptor.getLastEvent();
            String activationToken = event.getActivationToken().getToken();
            tenantCommandService.activate(activationToken, randomAlphabetic(5), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
            tenantCommandService.update(event.getTenantId(), randomAlphabetic(5), randomAlphabetic(5), randomAlphabetic(10), "EN", "RUS");
        }

        for (int i = 0; i < 10; i++) {
            String email = UserRandomUtil.getRandomEmail();
            tenantCommandService.signUp(randomAlphabetic(5), email);
        }
    }

}
