/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.core.util;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 10.08.13 : 13:07
 */
public abstract class ApplicationEventsInterceptor<E extends ApplicationEvent> implements ApplicationListener<E> {

    private E event;


    public void onApplicationEvent(E event) {
        this.event = event;
    }

    public void clear() {
        this.event = null;
    }

    public E getLastEvent() {
        if (event == null) {
            throw new IllegalStateException("no event caught");
        }
        return event;
    }
}
