/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.email;

import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.AbstractTenantTests;
import com.clusterra.iam.core.application.group.DefaultGroup;
import com.clusterra.iam.core.application.tenant.event.TenantSignedUpEvent;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.application.user.UserQueryService;
import com.clusterra.iam.core.testutil.UserRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 09.11.13
 */
public class TenantActivatedEvent4DefaultAdminListener_Test extends AbstractTenantTests {

    @Autowired
    private UserQueryService userQueryService;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        tenantSignedUpEventInterceptor.clear();
        signUpRandomTenant();
    }

    @Test
    public void test_activated_customer_account_has_admin_user() throws Exception {
        TenantSignedUpEvent event = getLastTenantSignedUpEvent();

        tenantCommandService.activate(event.getActivationToken().getToken(), randomAlphabetic(5), UserRandomUtil.randomPassword(), randomAlphabetic(5), randomAlphabetic(5));
        TenantId tenantId = event.getTenantId();

        RoleDescriptor role = roleService.findRoleByName(tenantId, DefaultRole.ADMIN);
        User admin = userQueryService.findByEmail(event.getEmail());
        GroupDescriptor groupDescriptor = groupService.findGroupByName(tenantId, DefaultGroup.DEFAULT.getName());
        assertThat(authorizedMembershipService.findRolesByGroupAndUser(groupDescriptor, new UserId(admin.getId())), hasItem(role));
    }


}
