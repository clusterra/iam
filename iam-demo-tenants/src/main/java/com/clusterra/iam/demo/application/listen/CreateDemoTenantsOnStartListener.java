/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.demo.application.listen;

import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.application.user.LoginAlreadyExistsException;
import com.clusterra.iam.core.application.role.RoleAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.demo.application.DemoTenantService;
import com.clusterra.iam.demo.application.config.TenantConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Denis Kuchugurov
 * Date: 01.10.13
 * Time: 10:40
 */
@Component
public class CreateDemoTenantsOnStartListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private DemoTenantService demoTenantService;

    @Autowired
    private List<TenantConfig> tenantConfigs;


    public void onApplicationEvent(ContextRefreshedEvent event) {
        for (TenantConfig config : tenantConfigs) {
            try {
                demoTenantService.create(config);
            } catch (InvalidTenantNameException | EmailAlreadyExistsException | TenantAlreadyExistsException | InvalidEmailException | RoleAlreadyExistsException | LoginAlreadyExistsException | TenantNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
