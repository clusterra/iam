/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.demo.application;

import com.clusterra.iam.avatar.application.AvatarId;
import com.clusterra.iam.avatar.application.AvatarImageResizeException;
import com.clusterra.iam.avatar.application.AvatarService;
import com.clusterra.iam.avatar.domain.model.AvatarType;
import com.clusterra.iam.core.application.group.GroupAlreadyExistsException;
import com.clusterra.iam.core.application.group.GroupDescriptor;
import com.clusterra.iam.core.application.group.GroupService;
import com.clusterra.iam.core.application.membership.AuthorizedMembershipService;
import com.clusterra.iam.core.application.role.ActionAlreadyAllowedException;
import com.clusterra.iam.core.application.role.ActionAlreadyExistsException;
import com.clusterra.iam.core.application.role.ActionDescriptor;
import com.clusterra.iam.core.application.role.ActionService;
import com.clusterra.iam.core.application.role.RoleDescriptor;
import com.clusterra.iam.core.application.role.RoleNotFoundException;
import com.clusterra.iam.core.application.tenant.InvalidTenantNameException;
import com.clusterra.iam.core.application.tenant.TenantAlreadyExistsException;
import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.event.TenantActivatedEvent;
import com.clusterra.iam.core.application.user.DefaultRole;
import com.clusterra.iam.core.application.user.EmailAlreadyExistsException;
import com.clusterra.iam.core.application.user.InvalidEmailException;
import com.clusterra.iam.core.application.user.LoginAlreadyExistsException;
import com.clusterra.iam.core.application.user.UserCommandService;
import com.clusterra.iam.core.application.user.UserId;
import com.clusterra.iam.core.domain.model.tenant.Tenant;
import com.clusterra.iam.core.domain.model.user.User;
import com.clusterra.iam.core.protect.ProtectedConstants;
import com.clusterra.iam.demo.application.config.TenantConfig;
import com.clusterra.iam.demo.application.config.UserConfig;
import org.apache.commons.lang3.StringUtils;
import com.clusterra.iam.core.application.role.RoleAlreadyExistsException;
import com.clusterra.iam.core.application.role.RoleService;
import com.clusterra.iam.core.application.tenant.TenantCommandService;
import com.clusterra.iam.core.application.tenant.TenantNotFoundException;
import com.clusterra.iam.core.application.tenant.TenantQueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 13.11.13
 */
@Service
public class DemoTenantServiceImpl implements DemoTenantService {

    private static Logger logger = LoggerFactory.getLogger(DemoTenantServiceImpl.class);

    @Autowired
    private UserCommandService userCommandService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TenantQueryService tenantQueryService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private AuthorizedMembershipService authorizedMembershipService;

    @Autowired
    private ActionService actionService;

    @Autowired
    private TenantCommandService tenantCommandService;

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Transactional
    public void create(TenantConfig config) throws EmailAlreadyExistsException, TenantAlreadyExistsException, InvalidEmailException, RoleAlreadyExistsException, LoginAlreadyExistsException, TenantNotFoundException, InvalidTenantNameException {

        if (tenantQueryService.isNameTaken(config.getName())) {
            logger.info("skipping tenant creation '{}' as it already exists", config.getName());
            return;
        }

        Tenant tenant = tenantCommandService.create(config.getName(), config.getAdmin().getEmail());
        publisher.publishEvent(
                new TenantActivatedEvent(this,
                        new TenantId(tenant.getId()),
                        config.getAdmin().getLogin(),
                        config.getAdmin().getPassword(),
                        config.getAdmin().getEmail(),
                        config.getAdmin().getFirstName(),
                        config.getAdmin().getLastName())
        );

        TenantId tenantId = new TenantId(tenant.getId());
        try {
            AvatarId avatarId = avatarService.newAvatar(AvatarType.TENANT, config.getAvatarResource());
            tenantCommandService.updateAvatar(tenantId, avatarId.getId());
        } catch (AvatarImageResizeException e) {
            throw new RuntimeException(e);
        }
        for (UserConfig userConfig : config.getUserConfigs()) {
            User user = userCommandService.create(tenantId, userConfig.getLogin(), userConfig.getEmail(), userConfig.getPassword(), userConfig.getFirstName(), userConfig.getLastName());
            UserId userId = new UserId(user.getId());
            logger.info("user login={} for tenant={} created...", userConfig.getLogin(), config.getName());
            List<String> roles = Arrays.asList(StringUtils.split(userConfig.getRoles(), ","));
            //TODO un-random
            GroupDescriptor group = null;
            try {
                group = groupService.createGroup(tenantId, tenant.getName() + randomAlphabetic(5));
            } catch (GroupAlreadyExistsException e) {
                throw new RuntimeException(e);
            }
            for (String role : roles) {
                RoleDescriptor roleDescriptor = setupRole(tenantId, role);


                authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, roleDescriptor, group);
                logger.info("role name={}, user={}, tenant={} assigned...", roleDescriptor.getRoleName(), user.getLogin(), tenant.getName());
            }
            RoleDescriptor userRole = roleService.findOrCreateRole(tenantId, DefaultRole.USER);
            authorizedMembershipService.createAuthorizedMembershipIfNotExists(tenantId, userId, userRole, group);
        }
    }

    public RoleDescriptor setupRole(TenantId tenantId, String role) {
        try {
            RoleDescriptor roleDescriptor = createRoleIfNotExists(tenantId, role);
            allowActionsForRole(roleDescriptor, Arrays.asList(ProtectedConstants.ACTION_SEE_PERSONAL_DETAILS));
            return roleDescriptor;
        } catch (RoleAlreadyExistsException | RoleNotFoundException | ActionAlreadyExistsException | ActionAlreadyAllowedException e) {
            throw new RuntimeException(e);
        }
    }

    private void allowActionsForRole(RoleDescriptor role, List<String> actionNames) throws ActionAlreadyExistsException, ActionAlreadyAllowedException {
        for (String actionName : actionNames) {
            ActionDescriptor action = createActionIfNotExists(actionName);
            if (!actionService.isActionAllowed(action, role)) {
                actionService.allowActionForRole(action, role);
            }
        }
    }

    private RoleDescriptor createRoleIfNotExists(TenantId tenant, String roleName) throws RoleAlreadyExistsException, RoleNotFoundException {
        if (!roleService.isRoleNameTaken(tenant, roleName)) {
            logger.info("creating new role:" + roleName);
            return roleService.createRole(tenant, roleName);
        }
        return roleService.findRoleByName(tenant, roleName);
    }

    private ActionDescriptor createActionIfNotExists(String actionName) throws ActionAlreadyExistsException {
        ActionDescriptor action = actionService.findActionByName(actionName);
        if (action == null) {
            logger.info("creating new action:" + actionName);
            return actionService.createAction(actionName);
        }
        return action;
    }
}
