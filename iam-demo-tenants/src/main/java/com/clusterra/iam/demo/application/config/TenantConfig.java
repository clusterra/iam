/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.demo.application.config;

import org.springframework.core.io.Resource;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Denis Kuchugurov
 * Date: 01.10.13
 * Time: 10:58
 */
public class TenantConfig {

    private final String name;

    private final String description;

    private final Resource avatarResource;

    private final UserConfig admin;

    private final List<UserConfig> userConfigs;

    public TenantConfig(String name, String description, Resource avatarResource, UserConfig admin, List<UserConfig> userConfigs) {
        this.name = name;
        this.description = description;
        this.avatarResource = avatarResource;
        this.admin = admin;
        this.userConfigs = userConfigs;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public UserConfig getAdmin() {
        return admin;
    }

    public List<UserConfig> getUserConfigs() {
        return userConfigs;
    }

    public Resource getAvatarResource() {
        return avatarResource;
    }
}
