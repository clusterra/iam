<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <title> ${msg("message.email.user-invited.subject")} </title>
</head>
<body>
<table style="width: 800px; margin: 0 auto;" align="center">
    <tr>
        <td style="background: #333;">
            <h3 style="float:right; color: #ffffff; margin-right:25px;">
                ${msg("message.email.user-invited.subject")}
            </h3>
            <img style="margin-left: 25px; padding-top: 18px; padding-bottom:14px;" src="cid:img/email/logo.png">
        </td>
        <td>
    </tr>
    <tr>
        <td>
            <div class="content" style="margin-top: 20px; margin-left: 10px; margin-right: 10px;">
                <h3>${msg("message.email.greeting")}</h3>

                <p>${msg("message.email.user-invited.body")}</p>

                <a href="${host}/#activate/${activationToken}">${host}/#activate/${activationToken}</a>

                <p>${msg("message.email.footer")}</p>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
