/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.email.listen;

import com.clusterra.email.sender.EmailSender;
import com.clusterra.freemarker.renderer.FreemarkerTemplateRenderer;
import com.clusterra.iam.core.application.tenant.event.TenantSignedUpEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Denis Kuchugurov
 * on 19/10/14 23:42.
 */
@Component
public class TenantSignedUpEvent4EmailListener implements ApplicationListener<TenantSignedUpEvent> {


    @Autowired
    private EmailSender emailSender;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    public void onApplicationEvent(TenantSignedUpEvent event) {
        Map<String, Object> variables = new HashMap<>();
        variables.put("activationToken", event.getActivationToken().getToken());

        String messageSubject = messageSource.getMessage("message.email.tenant-signed-up.subject", null, LocaleContextHolder.getLocale());
        String messageBody = freemarkerTemplateRenderer.render("tenant-signed-up.ftl", variables);
        emailSender.send(event.getEmail(), messageSubject, messageBody);
    }
}
