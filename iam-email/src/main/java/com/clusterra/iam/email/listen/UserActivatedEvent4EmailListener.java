/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.email.listen;

import com.clusterra.email.sender.EmailSender;
import com.clusterra.freemarker.renderer.FreemarkerTemplateRenderer;
import com.clusterra.iam.core.application.user.event.UserActivatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by kepkap on 19/10/14.
 */
@Component
public class UserActivatedEvent4EmailListener implements ApplicationListener<UserActivatedEvent> {

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    @Autowired
    private MessageSource messageSource;


    public void onApplicationEvent(UserActivatedEvent event) {
        Map<String, Object> variables = new HashMap<>();
        variables.put("login", event.getLogin());

        String messageSubject = messageSource.getMessage("message.email.user-activated.subject", null, LocaleContextHolder.getLocale());
        String messageBody = freemarkerTemplateRenderer.render("user-activated.ftl", variables);
        emailSender.send(event.getEmail(), messageSubject, messageBody);
    }


}
