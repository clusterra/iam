/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.email.listen;

import com.clusterra.iam.core.application.tenant.TenantId;
import com.clusterra.iam.core.application.tenant.event.TenantSignedUpEvent;
import com.clusterra.iam.core.domain.model.Token;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class
)
public class TenantSignUpEmailTemplateTest extends AbstractTestNGSpringContextTests {

    @Autowired
    @Qualifier("emailInboundChannel")
    private MessageChannel emailInboundChannel;

    @Autowired
    private ApplicationEventPublisher publisher;

    private ChannelHandlerHolder channelHandlerHolder;

    @BeforeMethod
    public void setup() {

        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request, new MockHttpServletResponse()));

        channelHandlerHolder = new ChannelHandlerHolder();

        PublishSubscribeChannel publishSubscribeChannel = (PublishSubscribeChannel) emailInboundChannel;
        publishSubscribeChannel.subscribe(new MessageHandler() {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException {
                channelHandlerHolder.setFlag(true);
            }
        });
    }

    @Test(enabled = false)
    public void test_template() throws Exception{
        publisher.publishEvent(
                new TenantSignedUpEvent(this,
                        new TenantId(randomAlphabetic(10)),
                        new Token(DateTime.now().plusDays(1).toDate()),
                        "kuchugurov@yandex.ru"
                )
        );

        assertThat(channelHandlerHolder.getFlag(), is(true));
        Thread.sleep(15000l);

    }


}