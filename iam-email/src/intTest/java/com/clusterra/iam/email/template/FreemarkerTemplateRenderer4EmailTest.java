/*
 * Copyright (c) 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.clusterra.iam.email.template;

import com.clusterra.freemarker.renderer.FreemarkerTemplateRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.web.WebAppConfiguration;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Denis Kuchugurov
 * on 08/04/15 23:17.
 */
@ContextConfiguration(
        value = {"classpath*:META-INF/spring/*.xml"},
        initializers = ConfigFileApplicationContextInitializer.class
)
@WebAppConfiguration
public class FreemarkerTemplateRenderer4EmailTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private FreemarkerTemplateRenderer freemarkerTemplateRenderer;

    @Test
    public void test_user_activated_template() throws Exception {
        Map<String, Object> variables = new HashMap<>();

        variables.put("host", "localhost");
        String render = freemarkerTemplateRenderer.render("user-activated.ftl", variables);

        System.out.println(render);
    }

    @Test
    public void test_user_invited_template() throws Exception {
        Map<String, Object> variables = new HashMap<>();

        variables.put("host", "localhost");
        variables.put("activationToken", "ASD");
        String render = freemarkerTemplateRenderer.render("user-invited.ftl", variables);

        System.out.println(render);
    }

    @Test
    public void test_tenant_signed_up_template() throws Exception {
        Map<String, Object> variables = new HashMap<>();

        variables.put("host", "localhost");
        variables.put("activationToken", "ASD");
        String render = freemarkerTemplateRenderer.render("tenant-signed-up.ftl", variables);

        System.out.println(render);
    }

    @Test
    public void test_password_change_requested_template() throws Exception {
        Map<String, Object> variables = new HashMap<>();

        variables.put("host", "localhost");
        variables.put("passwordToken", "ASD");
        String render = freemarkerTemplateRenderer.render("change-password-requested.ftl", variables);

        System.out.println(render);
    }

    @Test
    public void test_password_change_confirmed_template() throws Exception {
        Map<String, Object> variables = new HashMap<>();

        variables.put("host", "localhost");
        String render = freemarkerTemplateRenderer.render("change-password-confirmed.ftl", variables);

        System.out.println(render);
    }
}